Sample Gameboy emulator I wrote with a Vulkan backend.
This was a learning project, so there's a TON of problems
with it, but it does sorta load Tetris. Highly recommended
not to use this as an example for any of your own code, but 
if you do, let's say it's BSD licensed.

The Vulkan code is based on the Vulkan tutorial, and there's
a DAA implementation from elsewhere (documented in the code),
so those may have their own license restrictions.

![picture](img/screenshot.png)
