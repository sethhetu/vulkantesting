//////////////////////////////////////////////////////////
// This file contains helpers for manipulating bits and
// bytes, as well as the various opcode implementations
// This is meant to be used by cpu.cpp only, so it assumes
// some things about the internal structures and variables
// available to it.
//////////////////////////////////////////////////////////

#pragma once

// Set a flag
// TODO: Faster arithmetic op?
#define FSET_FLAG(bit, test) F = test ? (F | (1<<bit)) : (F & ~(1<<bit))

// Set a specific flag
#define FSET_Z(test) FSET_FLAG(7, test)
#define FSET_N(test) FSET_FLAG(6, test)
#define FSET_H(test) FSET_FLAG(5, test)
#define FSET_C(test) FSET_FLAG(4, test)

// Get a flag
#define FGET_FLAG(bit) (F&(1<<bit))

// Get a specific flag
#define FGET_Z() FGET_FLAG(7)
#define FGET_N() FGET_FLAG(6)
#define FGET_H() FGET_FLAG(5)
#define FGET_C() FGET_FLAG(4)

// Interpret an unsigned byte as a signed byte
#define SIGNED_8(val) (*((char*)((unsigned char*)&val)))

// Helper for retrieving a flag when you're not inside a define
inline bool flag_z(unsigned char F) { return FGET_Z(); }
inline bool flag_n(unsigned char F) { return FGET_N(); }
inline bool flag_h(unsigned char F) { return FGET_H(); }
inline bool flag_c(unsigned char F) { return FGET_C(); }

// Set/clear a flag; helper when you're not inside a define
inline void set_flag_z(unsigned char& F, bool val) { FSET_Z(val); }
inline void set_flag_n(unsigned char& F, bool val) { FSET_N(val); }
inline void set_flag_h(unsigned char& F, bool val) { FSET_H(val); }
inline void set_flag_c(unsigned char& F, bool val) { FSET_C(val); }


// Grumble, grumble, DAA...
// Sample code from: https://forums.nesdev.com/viewtopic.php?f=20&t=15944
// ...but I'd rather do it differently.
inline void DO_DAA_8(unsigned char& dst, unsigned char& F) {
	if (!FGET_N()) {
		// If addition:
		if (FGET_C() || dst > 0x99) { dst += 0x60; FSET_C(1); }
		if (FGET_H() || (dst & 0x0F) > 0x09) { dst += 0x6; }
	}
	else {
		// If subtraction:
		if (FGET_C()) { dst -= 0x60; }
		if (FGET_H()) { dst -= 0x6; }
	}
	FSET_Z(dst == 0);
	FSET_H(0);
}

// Shared operations: Increment
inline void DO_INC_8(unsigned char& dst, unsigned char& F) {
	// Increment, set flags
	FSET_H((dst & 0xF) == 0xF); // 0bXXXX1111 is the only thing that can half-carry
	dst++;
	FSET_Z(dst == 0);
	FSET_N(0);
}
inline void DO_INC_16(uint16_t& dst) {
	// Increment; doesn't set flags
	dst++;
}

// Shared operations: Decrement
inline void DO_DEC_8(unsigned char& dst, unsigned char& F) {
	// Decrement, set flags
	FSET_H(!(dst & 0xF)); // 0bXXXX0000 is the only thing that can half-borrow
	dst--;
	FSET_Z(dst == 0);
	FSET_N(1);
}
inline void DO_DEC_16(uint16_t& dst) {
	// Increment; doesn't set flags
	dst--;
}

// Shared operations: Load
inline void DO_LOAD_8(unsigned char& dst, unsigned char src) {
	// Perform the load; no flags are affected.
	dst = src;
}
inline void DO_LOAD_16(unsigned char& msbDST, unsigned char& lsbDST, unsigned char msbSRC, unsigned char lsbSRC) {
	// Perform the load; no flags are affected.
	msbDST = msbSRC;
	lsbDST = lsbSRC;
}


// Share operation: Roatate Left Carry
// TODO: Rotate operations seem wrong (and docs disagree)
// TODO: RLCA does NOT seem to be the same as RLC A
inline void DO_RLC_8(unsigned char& dst, unsigned char& F) {
	// Perform the rotate; update flags
	bool carry = dst & 0x80;
	dst = (dst << 1) | FGET_C();
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
	FSET_C(carry);
}
inline void DO_RL_8(unsigned char& dst, unsigned char& F) {
	// Perform the rotate; update flags
	bool carry = dst & 0x80;
	dst = (dst << 1) | ((dst >> 7) & 0x1);
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
	FSET_C(carry);
}

// Shared operation: Rotate Right Carry
inline void DO_RRC_8(unsigned char& dst, unsigned char& F) {
	// Perform the rotate; update flags
	bool carry = dst & 0x1;
	dst = ((dst >> 1) & 0x7F) | (FGET_C() << 7);
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
	FSET_C(carry);
}
inline void DO_RR_8(unsigned char& dst, unsigned char& F) {
	// Perform the rotate; update flags
	bool carry = dst & 0x1;
	dst = ((dst >> 1) & 0x7F) | (((dst & 0x80) >> 7) & 0x1);
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
	FSET_C(carry);
}


// Shared operation: Shifts
inline void DO_SLA_8(unsigned char& dst, unsigned char& F) {
	// Perform the shift; update flags
	FSET_C(dst & 0x80);
	dst = dst << 1;
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
}
inline void DO_SRA_8(unsigned char& dst, unsigned char& F) {
	// Perform the shift; update flags
	FSET_C(dst & 0x1);
	dst = (dst >> 1) | (dst & 0x80);
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
}
inline void DO_SRL_8(unsigned char& dst, unsigned char& F) {
	// Perform the shift; update flags
	FSET_C(dst & 0x1);
	dst = (dst >> 1) & 0x7F;
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
}


// Shared operation: Swap
inline void DO_SWAP_8(unsigned char& dst, unsigned char& F) {
	// Perform the swap; update flags
	dst = ((dst >> 4) & 0xF) | ((dst & 0xF) << 4);
	FSET_Z(dst == 0);
	FSET_N(0);
	FSET_H(0);
	FSET_C(0);
}

// Shared operation: Get Bit
inline void GET_BIT_AT_N(unsigned char src, unsigned char bit, unsigned char& F) {
	// Perform the operation; update flags
	FSET_Z((src & (0x1 << bit)) == 0);
	FSET_N(0);
	FSET_H(1);
}
inline void GET_BIT_AT_0(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 0, F); }
inline void GET_BIT_AT_1(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 1, F); }
inline void GET_BIT_AT_2(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 2, F); }
inline void GET_BIT_AT_3(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 3, F); }
inline void GET_BIT_AT_4(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 4, F); }
inline void GET_BIT_AT_5(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 5, F); }
inline void GET_BIT_AT_6(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 6, F); }
inline void GET_BIT_AT_7(unsigned char src, unsigned char& F) { GET_BIT_AT_N(src, 7, F); }


// Shared operation: Reset Bit
inline void RESET_BIT_AT_N(unsigned char& dst, unsigned char bit, unsigned char& F) {
	// Perform the operation; no flags updated
	dst &= (~(0x1 << bit));
}
inline void RESET_BIT_AT_0(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 0, F); }
inline void RESET_BIT_AT_1(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 1, F); }
inline void RESET_BIT_AT_2(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 2, F); }
inline void RESET_BIT_AT_3(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 3, F); }
inline void RESET_BIT_AT_4(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 4, F); }
inline void RESET_BIT_AT_5(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 5, F); }
inline void RESET_BIT_AT_6(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 6, F); }
inline void RESET_BIT_AT_7(unsigned char& dst, unsigned char& F) { RESET_BIT_AT_N(dst, 7, F); }


// Shared operation: Set Bit
inline void SET_BIT_AT_N(unsigned char& dst, unsigned char bit, unsigned char& F) {
	// Perform the operation; no flags updated
	dst |= (0x1 << bit);
}
inline void SET_BIT_AT_0(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 0, F); }
inline void SET_BIT_AT_1(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 1, F); }
inline void SET_BIT_AT_2(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 2, F); }
inline void SET_BIT_AT_3(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 3, F); }
inline void SET_BIT_AT_4(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 4, F); }
inline void SET_BIT_AT_5(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 5, F); }
inline void SET_BIT_AT_6(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 6, F); }
inline void SET_BIT_AT_7(unsigned char& dst, unsigned char& F) { SET_BIT_AT_N(dst, 7, F); }




// Shared operation: Add
inline void DO_ADD_16U(uint16_t& dst, uint16_t src, unsigned char& F) {
	// Perform the add; update flags
	// TODO: Use ADD and ADC to implement this?
	uint16_t orig = dst;
	dst += src;
	FSET_N(0);
	FSET_H((orig & 0xFFF) > (dst & 0xFFF)); // We half-carry if the resulting upper bits are less
	FSET_C(orig > dst); // We carry if the result is less
}
inline void DO_ADD_8U(unsigned char& dst, unsigned char src, unsigned char& F) {
	// Perform the add; update flags
	uint16_t orig = dst;
	dst += src;
	FSET_N(0);
	FSET_H((orig & 0xF) > (dst & 0xF)); // We half-carry if the resulting upper bits are less
	FSET_C(orig > dst); // We carry if the result is less
}
inline void DO_ADC_8U(unsigned char& dst, unsigned char src, unsigned char& F) {
	// Perform the add (use carry flag); update flags
	uint16_t orig = dst;
	dst += src + FGET_C();
	FSET_N(0);
	FSET_H((orig & 0xF) > (dst & 0xF)); // We half-carry if the resulting upper bits are less
	FSET_C(orig > dst); // We carry if the result is less
}

// Shared operation: Subtract & Compare
inline void DO_SUB_8U(unsigned char& dst, unsigned char src, unsigned char& F) {
	// Perform the add; update flags
	FSET_C(dst < src); // We need to borrow in this case.
	FSET_H((dst & 0xF) < (src & 0xF)); // We need to half-borrow in this case
	dst -= src;
	FSET_N(1);
	FSET_Z(dst == 0);
}
inline void DO_SBC_8U(unsigned char& dst, unsigned char src, unsigned char& F) {
	// Perform the add; update flags
	bool carry = FGET_C();
	FSET_C(dst < src); // We need to borrow in this case.
	FSET_H((dst & 0xF) < (src & 0xF)); // We need to half-borrow in this case
	dst -= src + carry;
	FSET_N(1);
	FSET_Z(dst == 0);
}
inline void DO_CP_8(unsigned char& dst, unsigned char src, unsigned char& F) {
	// No add performed; just update flags
	FSET_C(dst < src); // We need to borrow in this case.
	FSET_H((dst & 0xF) < (src & 0xF)); // We need to half-borrow in this case
	FSET_N(1);
	FSET_Z((dst - src) == 0);
}

// Shared operations: and, or, xor
inline void DO_AND_8(unsigned char& dst, unsigned char src, unsigned char& F) {
	// Perform the add; update flags
	dst &= src;
	FSET_Z(dst == 0);
}
inline void DO_OR_8(unsigned char& dst, unsigned char src, unsigned char& F) {
	// Perform the add; update flags
	dst |= src;
	FSET_Z(dst == 0);
}
inline void DO_XOR_8(unsigned char& dst, unsigned char src, unsigned char& F) {
	// Perform the add; update flags
	dst ^= src;
	FSET_Z(dst == 0);
}

// Shared operation: Complement
inline void DO_CPL_8(unsigned char& dst, unsigned char& F) {
	// Perform the complement; update flags
	dst = ~dst;
	FSET_N(1);
	FSET_H(1);
}

