//////////////////////////////////////////////////////////
// This file contains the cycle time and opcode length
// for each operation in the gameboy's CPU.
//////////////////////////////////////////////////////////

#pragma once

// First byte: number of operands (can be 0)
// Second byte: number of cycles (usually >= 4)
// TODO: RST is 16 or 32 cycles?
// TODO: RET with C/NC/Z/NZ should take 20 cycles if it's acted on, 8 otherwise
//       Right now, this is just hard-coded later on.
const std::array<unsigned char, 256 * 2> cmd_ops_cycles = {
	// 0x00 .. 0x0F
	0,4,   // 0x00 Noop
	2,12,  // 0x01 LD BC, nn
	0,8,   // 0x02 LD (BC), A
	0,8,   // 0x03 INC BC
	0,4,   // 0x04 INC B
	0,4,   // 0x05 DEC B
	1,8,   // 0x06 LD B, n
	0,4,   // 0x07 RLC A
	2,20,  // 0x08 LD (nn), SP
	0,8,   // 0x09 ADD HL, BC
	0,8,   // 0x0A LD A, (BC)
	0,8,   // 0x0B DEC BC
	0,4,   // 0x0C INC C
	0,4,   // 0x0D DEC C
	1,8,   // 0x0E LD C, n
	0,4,   // 0x0F RRC A

	// 0x10 .. 0x1F
	0,4,   // 0x10 STOP
	2,12,  // 0x11 LD DE, nn
	0,8,   // 0x12 LD (DE), A
	0,8,   // 0x13 INC DE
	0,4,   // 0x14 INC D
	0,4,   // 0x15 DEC D
	1,8,   // 0x16 LD D, n
	0,7,   // 0x17 RL A
	1,8,   // 0x18 JR n
	0,8,   // 0x19 ADD HL, DE
	0,8,   // 0x1A LD A, (DE)
	0,8,   // 0x1B DEC DE
	0,4,   // 0x1C INC E
	0,4,   // 0x1D DEC E
	1,8,   // 0x1E LD E, n
	0,4,   // 0x1F RR A

	// 0x20 .. 0x2F
	1,8,   // 0x20 JR NZ, n
	2,12,  // 0x21 LD HL, nn
	0,8,   // 0x22 LD (HL+), A
	0,8,   // 0x23 INC HL
	0,4,   // 0x24 INC H
	0,4,   // 0x25 DEC H
	1,8,   // 0x26 LD H, n
	0,4,   // 0x27 DAA
	1,8,   // 0x28 JR Z, n
	0,8,   // 0x29 ADD HL, HL
	0,8,   // 0x2A LD A, (HL+)
	0,8,   // 0x2B DEC HL
	0,4,   // 0x2C INC L
	0,4,   // 0x2D DEC L
	1,8,   // 0x2E LD L, n
	0,4,   // 0x2F CPL

	// 0x30 .. 0x3F
	1,8,   // 0x30 JR NC, n
	2,12,  // 0x31 LD SP, nn
	0,8,   // 0x32 LD (HL-), A
	0,8,   // 0x33 INC SP 
	0,12,  // 0x34 INC (HL)
	0,12,  // 0x35 DEC (HL)
	1,12,  // 0x36 LD (HL), n
	0,4,   // 0x37 SCF
	1,8,   // 0x38 JR C, n
	0,8,   // 0x39 ADD HL, SP
	0,8,   // 0x3A LD A, (HL-)
	0,8,   // 0x3B DEC SP
	0,4,   // 0x3C INC A
	0,4,   // 0x3D DEC A
	1,8,   // 0x3E LD A, n
	0,4,   // 0x3F CCF

	// 0x40 .. 0x4F
	0,4,   // 0x40 LD B, B
	0,4,   // 0x41 LD B, C
	0,4,   // 0x42 LD B, D
	0,4,   // 0x43 LD B, E
	0,4,   // 0x44 LD B, H
	0,4,   // 0x45 LD B, L
	0,8,   // 0x46 LD B, (HL)
	0,4,   // 0x47 LD B, A
	0,4,   // 0x48 LD C, B
	0,4,   // 0x49 LD C, C
	0,4,   // 0x4A LD C, D
	0,4,   // 0x4B LD C, E
	0,4,   // 0x4C LD C, H
	0,4,   // 0x4D LD C, L
	0,8,   // 0x4E LD C, (HL)
	0,4,   // 0x4F LD C, A

	// 0x50 .. 0x5F
	0,4,   // 0x50 LD D, B
	0,4,   // 0x51 LD D, C
	0,4,   // 0x52 LD D, D
	0,4,   // 0x53 LD D, E
	0,4,   // 0x54 LD D, H
	0,4,   // 0x55 LD D, L
	0,8,   // 0x56 LD D, (HL)
	0,4,   // 0x57 LD D, A
	0,4,   // 0x58 LD E, B
	0,4,   // 0x59 LD E, C
	0,4,   // 0x5A LD E, D
	0,4,   // 0x5B LD E, E
	0,4,   // 0x5C LD E, H
	0,4,   // 0x5D LD E, L
	0,8,   // 0x5E LD E, (HL)
	0,4,   // 0x5F LD E, A

	// 0x60 .. 0x6F
	0, 4,   // 0x60 LD H, B
	0, 4,   // 0x61 LD H, C
	0, 4,   // 0x62 LD H, D
	0, 4,   // 0x63 LD H, E
	0, 4,   // 0x64 LD H, H
	0, 4,   // 0x65 LD H, L
	0, 8,   // 0x66 LD H, (HL)
	0, 4,   // 0x67 LD H, A
	0, 4,   // 0x68 LD L, B
	0, 4,   // 0x69 LD L, C
	0, 4,   // 0x6A LD L, D
	0, 4,   // 0x6B LD L, E
	0, 4,   // 0x6C LD L, H
	0, 4,   // 0x6D LD L, L
	0, 8,   // 0x6E LD L, (HL)
	0, 4,   // 0x6F LD L, A

	// 0x70 .. 0x7F
	0, 8,   // 0x70 LD (HL), B
	0, 8,   // 0x71 LD (HL), C
	0, 8,   // 0x72 LD (HL), D
	0, 8,   // 0x73 LD (HL), E
	0, 8,   // 0x74 LD (HL), H
	0, 8,   // 0x75 LD (HL), L
	0, 4,   // 0x76 HALT *note*
	0, 8,   // 0x77 LD (HL), A
	0, 4,   // 0x78 LD A, B
	0, 4,   // 0x79 LD A, C
	0, 4,   // 0x7A LD A, D
	0, 4,   // 0x7B LD A, E
	0, 4,   // 0x7C LD A, H
	0, 4,   // 0x7D LD A, L
	0, 8,   // 0x7E LD A, (HL)
	0, 4,   // 0x7F LD A, A

	// 0x80 .. 0x8F
	0, 4,   // 0x80 ADD A,B
	0, 4,   // 0x81 ADD A,C
	0, 4,   // 0x82 ADD A,D
	0, 4,   // 0x83 ADD A,E
	0, 4,   // 0x84 ADD A,H
	0, 4,   // 0x85 ADD A,L
	0, 8,   // 0x86 ADD A, (HL)
	0, 4,   // 0x87 ADD A,A
	0, 4,   // 0x88 ADC A,B
	0, 4,   // 0x89 ADC A,C
	0, 4,   // 0x8A ADC A,D
	0, 4,   // 0x8B ADC A,E
	0, 4,   // 0x8C ADC A,H
	0, 4,   // 0x8D ADC A,L
	0, 8,   // 0x8E ADC A,(HL)
	0, 4,   // 0x8F ADC A,A

	// 0x90 .. 0x9F
	0, 4,   // 0x90 SUB A,B
	0, 4,   // 0x91 SUB A,C
	0, 4,   // 0x92 SUB A,D
	0, 4,   // 0x93 SUB A,E
	0, 4,   // 0x94 SUB A,H
	0, 4,   // 0x95 SUB A,L
	0, 8,   // 0x96 SUB A,(HL)
	0, 4,   // 0x97 SUB A,A
	0, 4,   // 0x98 SBC A,B
	0, 4,   // 0x99 SBC A,C
	0, 4,   // 0x9A SBC A,D
	0, 4,   // 0x9B SBC A,E
	0, 4,   // 0x9C SBC A,H
	0, 4,   // 0x9D SBC A,L
	0, 8,   // 0x9E SBC A,(HL)
	0, 4,   // 0x9F SBC A,A

	// 0xA0 .. 0xAF
	0, 4,   // 0xA0 AND A,B
	0, 4,   // 0xA1 AND A,C
	0, 4,   // 0xA2 AND A,D
	0, 4,   // 0xA3 AND A,E
	0, 4,   // 0xA4 AND A,H
	0, 4,   // 0xA5 AND A,L
	0, 8,   // 0xA6 AND A,(HL)
	0, 4,   // 0xA7 AND A,A
	0, 4,   // 0xA8 XOR A,B
	0, 4,   // 0xA9 XOR A,C
	0, 4,   // 0xAA XOR A,D
	0, 4,   // 0xAB XOR A,E
	0, 4,   // 0xAC XOR A,H
	0, 4,   // 0xAD XOR A,L
	0, 8,   // 0xAE XOR A,(HL)
	0, 4,   // 0xAF XOR A,A

	// 0x10 .. 0xBF
	0, 4,   // 0xB0 OR A,B
	0, 4,   // 0xB1 OR A,C
	0, 4,   // 0xB2 OR A,D
	0, 4,   // 0xB3 OR A,E
	0, 4,   // 0xB4 OR A,H
	0, 4,   // 0xB5 OR A,L
	0, 8,   // 0xB6 OR A,(HL)
	0, 4,   // 0xB7 OR A,A
	0, 4,   // 0xB8 CP A,B
	0, 4,   // 0xB9 CP A,C
	0, 4,   // 0xBA CP A,D
	0, 4,   // 0xBB CP A,E
	0, 4,   // 0xBC CP A,H
	0, 4,   // 0xBD CP A,L
	0, 8,   // 0xBE CP A,(HL)
	0, 4,   // 0xBF CP A,A

	// 0xC0 .. 0xCF
	0, 8,   // 0xC0 RET NZ
	0, 12,  // 0xC1 POP BC
	2, 12,  // 0xC2 JP NZ, nn
	2, 12,  // 0xC3 JP nn
	2, 12,  // 0xC4 CALL NZ, nn
	0, 16,  // 0xC5 PUSH BC
	1, 8,   // 0xC6 ADD A, n
	0, 32,  // 0xC7 RST 00H
	0, 8,   // 0xC8 RET Z
	0, 8,   // 0xC9 RET
	2, 12,  // 0xCA JP Z, nn
	0, 4,   // 0xCB CB Lookup
	2, 12,  // 0xCC CALL Z, nn
	2, 12,  // 0xCD CALL nn
	1, 8,   // 0xCE ADC A, n
	0, 32,  // 0xCF RST 08H

	// 0xD0 .. 0xDF
	0, 8,   // 0xD0 RET NC
	0, 12,  // 0xD1 POP DE
	2, 12,  // 0xD2 JP NC, nn
	0, 0,   // 0xD3 <INVALID>
	2, 12,  // 0xD4 CALL NC, nn
	0, 16,  // 0xD5 PUSH DE
	1, 8,   // 0xD6 SUB A, n
	0, 32,  // 0xD7 RST 10H
	0, 8,   // 0xD8 RET C
	0, 8,   // 0xD9 RETI
	2, 12,  // 0xDA JP C, nn
	0, 0,   // 0xDB <INVALID>
	2, 12,  // 0xDC CALL C, nn
	0, 0,   // 0xDD <INVALID>
	1, 8,   // 0xDE SBC A,n
	0, 32,  // 0xDF RST 18H

	// 0xE0 .. 0xEF
	1, 12,  // 0xE0 LD (FF00+n), A
	0, 12,  // 0xE1 POP HL
	0, 8,   // 0xE2 LD (FF00+C), A 
	0, 0,   // 0xE3 <INVALID>
	0, 0,   // 0xE4 <INVALID>
	0, 16,  // 0xE5 PUSH HL
	1, 8,   // 0xE6 AND A, n
	0, 32,  // 0xE7 RST 20H
	1, 16,  // 0xE8 ADD SP, n
	0, 4,   // 0xE9 JP (HL)
	2, 16,  // 0xEA LD (nn), A
	0, 0,   // 0xEB <INVALID>
	0, 0,   // 0xEC <INVALID>
	0, 0,   // 0xED <INVALID>
	1, 8,   // 0xEE XOR A,n
	0, 32,  // 0xEF RST 28H

	// 0xF0 .. 0xFF
	1, 12,  // 0xF0 LD A, (FF00+n)
	0, 12,  // 0xF1 POP AF
	0, 8,   // 0xF2 LD A, (FF00+C)
	0, 4,   // 0xF3 DI
	0, 0,   // 0xF4 <INVALID>
	0, 16,  // 0xF5 PUSH AF
	1, 8,   // 0xF6 OR A,n
	0, 32,  // 0xF7 RST 30H
	1, 12,  // 0xF8 LD HL, SP+n
	0, 8,   // 0xF9 LD SP, HL
	2, 16,  // 0xFA LD A, (nn)
	0, 4,   // 0xFB EI
	0, 0,   // 0xFC <INVALID>
	0, 0,   // 0xFD <INVALID>
	1, 8,   // 0xFE CP A,n
	0, 32,  // 0xFF RST 38H
};
