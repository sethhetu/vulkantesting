#pragma once

// Grumble, grumble, vs...
#ifndef _ITERATOR_DEBUG_LEVEL
#define _ITERATOR_DEBUG_LEVEL 0
#endif

#include <bitset>
#include <sstream>
#include <array>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>


// State of the currently running program
struct RuntimeState {
	RuntimeState() : running(true), opsRun(0), cycles(0), bank(0), internalDiv(0), cyclesScanline(0), isVBlank(false) {}

	bool running;  // Is the program considered to be running?

	size_t opsRun;  // Handy helper; how many opcodes have we run?
	size_t cycles; // How many cycles since start?
	size_t bank; // Memory bank currently mapped in. (NOTE: Seems unused at the moment)

	size_t internalDiv; // DIV updates once every 16 cycles, so we need to track that here.

	size_t cyclesScanline; // Loops at 456; that means a scanline is done.
	bool isVBlank; // Single signal for vblank; once per frame
};


// Keys
enum class GB_BUTTONS {
	A,
	B,
	SELECT,
	START,
	RIGHT,
	LEFT,
	UP,
	DOWN,
};


// Which bank to map to, and how big it is
#define CART_MAPPED_SIZE 0x4000
enum class CART_MAPPED_BANK {
	ZERO,
	ONE,
};

// Tilemap banks
enum class TILEMAP_BANK {
	ZERO,
	ONE,
};

// Properties of the system
// Assuming 70224 cycles per LCD refresh
#define CYCLES_PER_SCANLINE 456
#define LCD_VBLANK_LINE 144
#define LCD_NUM_LINES 154
#define LCD_START_LINE 0x0

// Property of the Background layer
#define GB_BG_FULL_WIDTH 0x20
#define GB_BG_FULL_HEIGHT 0x20

// More properties of the Gameboy
#define GB_MAX_SPRITES 40
#define GB_RAM_MAX 0x10000


// Holds properties of the GameBoy CPU (as well as some domain-specific properties of the system).
class GB_CPU {
public:
	GB_CPU(unsigned char defaultRAMval = 0x00);

	// TODO: I think we can do this better.
	void set_zero_flag(unsigned char test);
	void set_zero_flag16(uint16_t test);

	// Shared operations: Stack manipulations
	void PUSH_STACK_16(uint16_t& sp, uint16_t val);
	uint16_t POP_STACK_16(uint16_t& sp);

	// Performs a number of memory checks
	void DO_CHECKED_LOAD_8(uint16_t addr, unsigned char src);

	// Run the emulator in "realtime" mode, which allows us to advance frame by frame.
	void setRealtime(bool val);

	// Force the code to stop after a specific address is reached.
	void setBreakpoint(uint16_t pos);

	// Log a message whenever a given PC is reached.
	// Handy for determining how we got there (Jump vs. Call, etc.)
	void traceProgramFlowTo(uint16_t pos);

	// Press a given button press after X cycles have passed, for a duration of Y cycles
	// Disabled when realtime mode is on.
	void addButtonPress(size_t fromCycle, size_t cycleLen, const std::string& btnName);

	// Helper: map part of the cartridge to a page
	void mapCartridgeToPage(CART_MAPPED_BANK bank, uint16_t page);

	// The 0xCB page
	void runCBPageCode(unsigned char code); 

	// The regular instruction set
	bool runOpcode(unsigned char operation, const uint16_t& pc, uint16_t& nxtPC, size_t& incr, unsigned char& opSize);

	// Run some code from a file
	void runFile(const std::string& filename, uint16_t default_PC = 0x0100);

	// Run some code directly (as a byte stream)
	void runCode(const std::vector<unsigned char>& program, uint16_t default_PC = 0x0000);

	void runProgram(uint16_t default_PC);

	// Sets the key state externally
	void setKeyState(const std::map<GB_BUTTONS, bool>& keys);

	// Run ops until the next vblank. Returns true if there was no error.
	bool runUntilNextVBlank();

	// Run a single opcode and return true if there was no error.
	bool runNextOp();

	// Handy debug helper
	void printAllRam() const;

	// Retrieve the background list of tiles (0x20 by 0x20), and normalize the tile IDs into the range of 
	// the full tilemap.
	std::vector<unsigned char> getBgTileMap() const;

	// Get Sprite map
	// Returns an array of OAM entries (for now just <X,Y,TileId,Flags>)
	std::vector<std::tuple<unsigned char, unsigned char, unsigned char, unsigned char>> getSprites() const;

	// Get tilemap data
	// Returns data as a vector of H rows of W colums
	// Each byte is a palette color (i.e., 0, 1, 2, 3)
	std::vector<unsigned char> getTilemap() const;

	// Used for detecting which tilemap changed.
	bool isFlagChanged(size_t tileId) const;
	void clearTilemapFlags();

	// Get the name of the ROM
	std::string getROMName() const;

private:
	// Helper: Consistent trace messages
	void logTrace(const std::string& msg);

	// Set quasi-sane startup values.
	void initMemoryRegisters();

	// Can be handy for printing
	std::string bits(unsigned char byt) const;
	
	// Single-width registers; matches layout in registers
	// NOTE: Targeting Intel for now (blah), so reverse every other register
	inline unsigned char& reg_F() { return registers.at(0); }
	inline unsigned char& reg_A() { return registers.at(1); }
	inline unsigned char& reg_C() { return registers.at(2); }
	inline unsigned char& reg_B() { return registers.at(3); }
	inline unsigned char& reg_E() { return registers.at(4); }
	inline unsigned char& reg_D() { return registers.at(5); }
	inline unsigned char& reg_L() { return registers.at(6); }
	inline unsigned char& reg_H() { return registers.at(7); }
	inline unsigned char& reg_P() { return registers.at(8); }
	inline unsigned char& reg_S() { return registers.at(9); }

	// Const variants
	inline const unsigned char& reg_F() const { return registers.at(0); }
	inline const unsigned char& reg_A() const { return registers.at(1); }
	inline const unsigned char& reg_C() const { return registers.at(2); }
	inline const unsigned char& reg_B() const { return registers.at(3); }
	inline const unsigned char& reg_E() const { return registers.at(4); }
	inline const unsigned char& reg_D() const { return registers.at(5); }
	inline const unsigned char& reg_L() const { return registers.at(6); }
	inline const unsigned char& reg_H() const { return registers.at(7); }
	inline const unsigned char& reg_P() const { return registers.at(8); }
	inline const unsigned char& reg_S() const { return registers.at(9); }


	// Two-byte registers use terrible alignment magic.
	inline uint16_t& reg_AF() { return *((uint16_t*)&registers.at(0)); }
	inline uint16_t& reg_BC() { return *((uint16_t*)&registers.at(2)); }
	inline uint16_t& reg_DE() { return *((uint16_t*)&registers.at(4)); }
	inline uint16_t& reg_HL() { return *((uint16_t*)&registers.at(6)); }
	inline uint16_t& reg_SP() { return *((uint16_t*)&registers.at(8)); }
	inline uint16_t& reg_PC() { return *((uint16_t*)&registers.at(10)); }

	// Memory-mapped registers

	// Used for reading gamepad info
	inline const unsigned char& memreg_P1() const { return memory[0xFF00]; }
	inline unsigned char& memreg_P1() { return memory[0xFF00]; }

	// Serial transfer data
	inline const unsigned char& memreg_SB() const { return memory[0xFF01]; }
	inline unsigned char& memreg_SB() { return memory[0xFF01]; }

	// Serial I/O control
	inline const unsigned char& memreg_SC() const { return memory[0xFF02]; }
	inline unsigned char& memreg_SC() { return memory[0xFF02]; }

	// Divide Register
	inline const unsigned char& memreg_DIV() const { return memory[0xFF04]; }
	inline unsigned char& memreg_DIV() { return memory[0xFF04]; }

	// Timer Counter
	inline const unsigned char& memreg_TIMA() const { return memory[0xFF05]; }
	inline unsigned char& memreg_TIMA() { return memory[0xFF05]; }

	// Timer Modulo
	inline const unsigned char& memreg_TMA() const { return memory[0xFF06]; }
	inline unsigned char& memreg_TMA() { return memory[0xFF06]; }

	// Timer Control
	inline const unsigned char& memreg_TAC() const { return memory[0xFF07]; }
	inline unsigned char& memreg_TAC() { return memory[0xFF07]; }

	// Interrupt flag: "which interrupt happened?"
	inline const unsigned char& memreg_IF() const { return memory[0xFF0F]; }
	inline unsigned char& memreg_IF() { return memory[0xFF0F]; }

	// The LCD controller
	inline const unsigned char& memreg_LCDC() const { return memory[0xFF40]; }
	inline unsigned char& memreg_LCDC() { return memory[0xFF40]; }

	// The LCD status
	inline const unsigned char& memreg_STAT() const { return memory[0xFF41]; }
	inline unsigned char& memreg_STAT() { return memory[0xFF41]; }

	// The BG Y Scroll position
	inline const unsigned char& memreg_SCY() const { return memory[0xFF42]; }
	inline unsigned char& memreg_SCY() { return memory[0xFF42]; }

	// The BG X Scroll position
	inline const unsigned char& memreg_SCX() const { return memory[0xFF43]; }
	inline unsigned char& memreg_SCX() { return memory[0xFF43]; }

	// The LCD Y position
	inline const unsigned char& memreg_LY() const { return memory[0xFF44]; }
	inline unsigned char& memreg_LY() { return memory[0xFF44]; }

	// The LCD Y compare
	inline const unsigned char& memreg_LYC() const { return memory[0xFF45]; }
	inline unsigned char& memreg_LYC() { return memory[0xFF45]; }

	// DMA Transfer & Start
	inline const unsigned char& memreg_DMA() const { return memory[0xFF46]; }
	inline unsigned char& memreg_DMA() { return memory[0xFF46]; }

	// BG/Window Palette
	inline const unsigned char& memreg_BGP() const { return memory[0xFF47]; }
	inline unsigned char& memreg_BGP() { return memory[0xFF47]; }

	// Object Palette 0
	inline const unsigned char& memreg_OBP0() const { return memory[0xFF48]; }
	inline unsigned char& memreg_OBP0() { return memory[0xFF48]; }

	// Object Palette 1
	inline const unsigned char& memreg_OBP1() const { return memory[0xFF49]; }
	inline unsigned char& memreg_OBP1() { return memory[0xFF49]; }

	// Window Y Position
	inline const unsigned char& memreg_WY() const { return memory[0xFF4A]; }
	inline unsigned char& memreg_WY() { return memory[0xFF4A]; }

	// Window X Position
	inline const unsigned char& memreg_WX() const { return memory[0xFF4B]; }
	inline unsigned char& memreg_WX() { return memory[0xFF4B]; }

	// Interrupt enable flag: "which interrupt do we process?"
	inline const unsigned char& memreg_IE() const { return memory[0xFFFF]; }
	inline unsigned char& memreg_IE() { return memory[0xFFFF]; }


	std::array<unsigned char, 12> registers;

	bool ime; // Master interrupt enable (seems to be part of the SOC?)

	std::vector<unsigned char> memory;    // Memory at runtime
	std::vector<unsigned char> cartridge; // Copy of the file.

	// Tracks the program while running. Mostly only useful in realtime mode.
	RuntimeState state;

	// Tracks if the tilemap has been changed via memory access.
	std::vector<bool> tileChanged;  // Any memory location for this tile changed this frame.
	bool tilemapChanged; // At least one tile changed.

	// Are we running in realtime, or as a batch process?
	bool realtime;

	// Keys the user is pressing
	std::map<GB_BUTTONS, bool> userKeys;

	// Tools for debugging
	uint16_t breakPoint; // 0x00 means "none"; otherwise, stop when we reach this address.
	uint16_t traceProgramFlowPoint; // 0x00 means "none"; otherwise, pring a message when this address is reached (and *how*)
	std::pair<size_t, size_t> btnPress; // fromCycle, cycleLen button presses

};

