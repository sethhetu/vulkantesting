#include "cpu.h"

#include <cstring>

#include "helper.h"
#include "ops_main.h"
#include "ops_cb.h"


#include "ops_main.ipp"
#include "ops_cb.ipp"


// TODO: The default value of RAM is not random:
//       Some mapped elements seem to be deterministic
//       Others are just "random", whatever that means for RAM.
GB_CPU::GB_CPU(unsigned char defaultRAMval) : 
	memory(GB_RAM_MAX, defaultRAMval), registers({ 0,0,0,0,0,0,0,0,0,0,0,0 }), 
	ime(false), tileChanged(0x180), tilemapChanged(false), realtime(false), breakPoint(0x0000)
{
}

void GB_CPU::set_zero_flag(unsigned char test)
{
	if (test == 0) {
		reg_F() = reg_F() | 0x80;
	} else {
		reg_F() = reg_F() & 0x7F;
	}
}

void GB_CPU::set_zero_flag16(uint16_t test)
{
	if (test == 0) {
		reg_F() = reg_F() | 0x80;
	} else {
		reg_F() = reg_F() & 0x7F;
	}
}

inline void GB_CPU::PUSH_STACK_16(uint16_t& sp, uint16_t val)
{
	sp -= 2;
	DO_CHECKED_LOAD_8(sp, val & 0xFF);
	DO_CHECKED_LOAD_8(sp + 1, (val >> 8) & 0xFF);
}

inline uint16_t GB_CPU::POP_STACK_16(uint16_t& sp)
{
	sp += 2;
	return memory[sp - 2] | (memory[sp - 1] << 8);
}


inline void GB_CPU::DO_CHECKED_LOAD_8(uint16_t addr, unsigned char src)
{
	if (addr < 0x8000) {
		// Cartridge ROM; the only thing we can do here is switch banks
		if ((addr<0x4000) && (addr >= 0x2000)) {
			std::cout << "Bank switched to: " << int(src) << std::endl;
			mapCartridgeToPage(CART_MAPPED_BANK::ONE, src);
		}
	} else {
		// Track tilemap changes
		if (addr >= 0x8000 && addr < 0x97FF) {
			tileChanged[(addr - 0x8000) / 0x10] = true;
			tilemapChanged = true;
		}

		// DIV register writes reset it
		if (addr == 0xFF04) {
			memreg_DIV() = 0;
		} else {
			// TODO: Some other things can't be written to, but we'll deal with them later.
			DO_LOAD_8(memory[addr], src);
		}
	}
}

void GB_CPU::setRealtime(bool val)
{
	realtime = val;
}

void GB_CPU::setBreakpoint(uint16_t pos)
{
	breakPoint = pos;
}

void GB_CPU::traceProgramFlowTo(uint16_t pos)
{
	traceProgramFlowPoint = pos;
}

void GB_CPU::addButtonPress(size_t fromCycle, size_t cycleLen, const std::string& btnName)
{
	// TODO: Add these to a list so we can have more than 1.
	btnPress.first = fromCycle;
	btnPress.second = fromCycle + cycleLen;
}

void GB_CPU::mapCartridgeToPage(CART_MAPPED_BANK bank, uint16_t page)
{
	// Compute offsets
	size_t bankStart = (bank == CART_MAPPED_BANK::ONE) ? 0x4000 : 0x0000;
	size_t memStart = page * CART_MAPPED_SIZE;
	size_t memEnd = memStart + CART_MAPPED_SIZE;

	// Safeties
	if (memStart > cartridge.size()) {
		std::cout << "WARNING: Can't map cartridge to slot " << (bank == CART_MAPPED_BANK::ONE ? "1" : "0") << " at all; skipping." << std::endl;
		return;
	}
	if (memEnd > cartridge.size()) {
		memEnd = cartridge.size();
		std::cout << "WARNING: Can't map entire cartridge to slot " << (bank == CART_MAPPED_BANK::ONE ? "1" : "0") << ", mapping as much as possible." << std::endl;
	}

	// Map
	std::memcpy(&memory[bankStart], &cartridge[memStart], memEnd - memStart);
}

void GB_CPU::runFile(const std::string& filename, uint16_t default_PC)
{
	// Read the file into a vector of bytes
	std::ifstream in(filename.c_str(), std::ios::binary | std::ios::ate);
	if (!in) {
		std::cout << "ERROR: Couldn't find file: " << filename << std::endl;
		throw std::runtime_error("Couldn't find file.");
	}
	auto pos = in.tellg();
	in.seekg(0, std::ios::beg);

	cartridge.resize(pos);
	in.read((char*)(cartridge.data()), pos);
	in.close();

	runProgram(default_PC);
}

void GB_CPU::runCode(const std::vector<unsigned char>& program, uint16_t default_PC)
{
	cartridge = program;
	runProgram(default_PC);
}


void GB_CPU::runProgram(uint16_t default_PC)
{
	// Map page 0 and page 1 to start
	mapCartridgeToPage(CART_MAPPED_BANK::ZERO, 0);
	mapCartridgeToPage(CART_MAPPED_BANK::ONE, 1);

	// Start up the program counter
	reg_PC() = default_PC;

	// Init the memory register sanely.
	initMemoryRegisters();

	std::cout << "Running GB program" << std::endl;

	// Program State is
	state = RuntimeState();

	// In "realtime" mode, we have to stop here and wait for the graphics card.
	if (realtime) {
		std::cout << "Program running in 'realtime' mode, handing control back to the Vulkan backend." << std::endl;
		return;
	}

	// If not in "realtime" mode, just run as fast as possible.
	while (reg_PC() < memory.size() && state.running) {
		state.running = runNextOp();
	}
	std::cout << "GB program exited normally" << std::endl;
}


void GB_CPU::setKeyState(const std::map<GB_BUTTONS, bool>& keys)
{
	this->userKeys = keys;
}

bool GB_CPU::runUntilNextVBlank()
{
	// This can be called even when we're done.
	if (!state.running) { return false; }

	while (reg_PC() < memory.size() && state.running) {
		state.running = runNextOp();
		if (state.isVBlank) {
			return state.running;
		}
	}

	return state.running;
}



bool GB_CPU::runNextOp()
{
	// Get the instruction and its size
	const uint16_t& pc = reg_PC(); // Placeholder
	unsigned char operation = memory[pc];
	unsigned char opSize = cmd_ops_cycles[operation * 2];
	size_t incr = cmd_ops_cycles[operation * 2 + 1];

	// Safety
	state.opsRun += 1;
	if (!realtime) {
		if (state.opsRun >= 0xFFFFFF) {
			std::cout << "Error: after " << state.opsRun << " operations, program has not exited or hit breakpoint" << std::endl;
			std::cout << "Current PC is: " << std::uppercase << std::setfill('0') << std::setw(4) << std::hex << reg_PC() << std::endl;
			return false;
		}
	}

	// Retrieve the next value of the program counter.
	// It's easier to manipuulate/save this way
	uint16_t nxtPC = pc + opSize + 1;
	uint16_t origNxtPc = nxtPC; // Before an operation can modify it.
								//std::cout << "OPERATION: " << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << (int(operation) & 0xFF) << std::endl;

								
	// Perform the operation
	runOpcode(operation, pc, nxtPC, incr, opSize);

	// React to DMAs
	if (memreg_DMA() != 0x0) {
		uint16_t dmaStart = memreg_DMA() << 8;
		memreg_DMA() = 0x0;

		// NOTE: Not sure what happens if the memory overlaps... but that shouldn't be possible (overwrite OAM with OAM?)
		memcpy(&memory[0xFE00], &memory[dmaStart], 0xA0);
	}

	// Count up cycles
	state.cycles += incr;

	// Update DIV register
	state.internalDiv += incr;
	if (state.internalDiv >= 16) {
		state.internalDiv -= 16;
		memreg_DIV() += 1;
	}

	// Count up LCD scanline cycles, and update LY
	state.cyclesScanline += incr;
	state.isVBlank = false;
	if (state.cyclesScanline >= CYCLES_PER_SCANLINE) {
		// New scanline
		state.cyclesScanline -= CYCLES_PER_SCANLINE;
		memreg_LY() += 1;

		// VBlank interrupt
		if (memreg_LY() == LCD_VBLANK_LINE) {
			state.isVBlank = true;
			if (ime && (memreg_IE() & 0x1)) {
				ime = false;
				//memreg_IF() |= 0x1; // Apparently not needed; this is used to check what happened?
				PUSH_STACK_16(reg_SP(), nxtPC);
				nxtPC = 0x40;
			}
		}

		// New frame
		if (memreg_LY() >= LCD_NUM_LINES) {
			memreg_LY() = 0;
		}
	}

	// Force simulated button presses here.
	// TODO: Right now, we only simulate Start
	if (!realtime) {
		// Auto-buttons don't work in realtime mode.
		userKeys.clear();
		if (state.opsRun >= btnPress.first &&state.opsRun <= btnPress.second) {
			userKeys[GB_BUTTONS::START] = true;
		}
	}

	// Input needs to be reset to *high*
	unsigned char lowBits = 0;
	if ((memreg_P1() & (0x1 << 4))) {
		// Build up the byte and flip it.
		if (userKeys[GB_BUTTONS::START]) { lowBits |= 0x8; }
		if (userKeys[GB_BUTTONS::SELECT]) { lowBits |= 0x4; }
		if (userKeys[GB_BUTTONS::B]) { lowBits |= 0x2; }
		if (userKeys[GB_BUTTONS::A]) { lowBits |= 0x1; }
		lowBits = ~lowBits;
	} else {
		if (userKeys[GB_BUTTONS::DOWN]) { lowBits |= 0x8; }
		if (userKeys[GB_BUTTONS::UP]) { lowBits |= 0x4; }
		if (userKeys[GB_BUTTONS::LEFT]) { lowBits |= 0x2; }
		if (userKeys[GB_BUTTONS::RIGHT]) { lowBits |= 0x1; }
		lowBits = ~lowBits;
	}
	memreg_P1() = (lowBits & 0x0F) | (memreg_P1() & 0xF0); // Enforce low/high bits

	// Trace
	if (traceProgramFlowPoint == nxtPC && origNxtPc == nxtPC) {
		logTrace("naturally.");
	}

	// Skip the instruction and its operands, account for jump
	reg_PC() = nxtPC;

	// Break?
	if (!realtime) {
		if (breakPoint != 0x0000 && breakPoint == reg_PC()) {
			std::cout << "Breakpoint reached: " << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << breakPoint << " after 0x" << std::uppercase << std::setfill('0') << std::hex << state.opsRun << " ops" << std::endl;
			return false;
		}
	}

	return true;
}


void GB_CPU::printAllRam() const
{
	// Print in a semi-usable block format
	std::ofstream out("ram.txt");

	// Print the registers first
	out << "Registers:\n";
	out << "  A = 0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << ((unsigned int)(reg_A()) & 0xFFFF) << "\n";
	out << "  B = 0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << ((unsigned int)(reg_B()) & 0xFFFF) << "\n";
	out << "  C = 0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << ((unsigned int)(reg_C()) & 0xFFFF) << "\n";
	out << "  D = 0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << ((unsigned int)(reg_D()) & 0xFFFF) << "\n";
	out << "  E = 0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << ((unsigned int)(reg_E()) & 0xFFFF) << "\n";
	out << "  F = 0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << ((unsigned int)(reg_F()) & 0xFFFF) << "\n";
	out << "Memory:"; // No newline; it's added automatically.

	int reset = 0;
	for (size_t i = 0; i < GB_RAM_MAX; i++) {
		// Print a row header
		if (reset <= 0) {
			out << std::endl << "0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << ((unsigned int)(i) & 0xFFFF) << ": ";
			reset = 0x100;
		}

		// Print the current value
		out << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << (int(memory[i]) & 0xFF) << " ";
		reset--;
	}
	out << std::endl;

	std::cout << "Ram is in file 'ram.txt'" << std::endl;
}

std::vector<unsigned char> GB_CPU::getBgTileMap() const
{
	// BG 0 is $9800-$9BFF
	// BG 1 is $9C00-$9FFF
	size_t mapStart = (memreg_LCDC()&(0x1 << 3)) ? 0x9C00 : 0x9800;

	// Tile Bank 0 is $8800-97FF, signed (TODO)
	// Tile Bank 1 is $8000-8FFF
	std::vector<unsigned char> res;
	bool tileBankFlag = memreg_LCDC()&(0x1 << 4);
	if (!tileBankFlag) {
		std::cout << "ERROR: Tile Bank $8800-97FF not properly handled yet" << std::endl;
		return res;
	}

	// Read each row/col
	for (size_t y = 0; y < GB_BG_FULL_HEIGHT; y++) {
		for (size_t x = 0; x < GB_BG_FULL_WIDTH; x++) {
			res.push_back(memory[mapStart++]);
		}
	}

	return res;
}


std::vector<std::tuple<unsigned char, unsigned char, unsigned char, unsigned char>> GB_CPU::getSprites() const
{
	std::vector<std::tuple<unsigned char, unsigned char, unsigned char, unsigned char>> res;
	for (uint16_t i = 0; i < GB_MAX_SPRITES; i++) {
		uint16_t start = 0xFE00 + 4 * i;
		std::tuple<unsigned char, unsigned char, unsigned char, unsigned char> item(memory[start], memory[start + 1], memory[start + 2], memory[start + 3]);
		if (std::get<0>(item) != 0 || std::get<1>(item) != 0) {
			res.push_back(item);
		}
	}
	return res;
}

std::vector<unsigned char> GB_CPU::getTilemap() const
{
	static std::vector<unsigned char> tilemap_res(0x6000); // Keep this in memory for faster operations.
														   // Has the tilemap not changed?
	if (!tilemapChanged) { return tilemap_res; }

	// Tile Bank 0 is 0x8000 to 0x8FFF
	// Tile Bank 1 is 0x8800 to 0x97FF
	// So we read 0x8000 to 0x97FF

	// Reach each of the 0x180 tiles
	for (size_t tileId = 0; tileId<0x180; tileId++) {
		if (!tileChanged[tileId]) { continue; }

		// Offsets
		size_t vectId = 64 * tileId;
		size_t tileStart = 0x8000 + 0x10 * tileId;

		// Each tile consists of 0x10 bytes
		for (size_t y = 0; y < 8; y++) {
			// Each line is 2 bytes
			unsigned char lsb = memory[tileStart];
			unsigned char msb = memory[tileStart + 1];
			tileStart += 2;

			// Decode the bits
			// TODO: Potentially unroll this loop.
			for (int x = 7; x >= 0; x--) {
				unsigned char pal = ((lsb&(0x1 << x)) ? 1 : 0) | ((msb&(0x1 << x) ? 2 : 0));
				tilemap_res[vectId++] = pal;
			}
		}
	}
	return tilemap_res;
}


bool GB_CPU::isFlagChanged(size_t tileId) const
{
	return tileChanged[tileId];
}

void GB_CPU::clearTilemapFlags()
{
	tilemapChanged = false;
	tileChanged = std::vector<bool>(0x180);
}

std::string GB_CPU::getROMName() const
{
	if (memory.size() <= 0x142) {
		return "";
	}

	std::stringstream res;
	for (size_t i = 0x134; i <= 0x142; i++) {
		char c = memory[i];
		if (c == '\0') {
			break;
		}
		res << c;
	}
	return res.str();
}

inline void GB_CPU::logTrace(const std::string& msg)
{
	std::cout << "TRACE: Address 0x" << std::uppercase << std::setfill('0') << std::setw(4) << std::hex << traceProgramFlowPoint << " was reached " << msg << " => from: " << std::uppercase << std::setfill('0') << std::setw(4) << std::hex << reg_PC() << std::endl;
}


void GB_CPU::initMemoryRegisters()
{
	memreg_P1() = 0xF; // Gamepad
	memreg_SB() = 0x0; // Serial I/O
	memreg_SC() = 0x0; // Serial I/O
	memreg_DIV() = 0x0; // Divide
	memreg_TIMA() = 0x0; // Timer
	memreg_TMA() = 0x0; // Timer Modulo
	memreg_TAC() = 0x0; // Timer Control
	memreg_IF() = 0x0; // Interrupt Flag

   //Sound stuff
	memory[0xFF10] = 0x0;
	memory[0xFF11] = 0x0;
	memory[0xFF12] = 0x0;
	memory[0xFF13] = 0x0;
	memory[0xFF14] = 0x0;
	memory[0xFF16] = 0x0;
	memory[0xFF17] = 0x0;
	memory[0xFF18] = 0x0;
	memory[0xFF19] = 0x0;
	memory[0xFF1A] = 0x0;
	memory[0xFF1B] = 0x0;
	memory[0xFF1C] = 0x0;
	memory[0xFF1D] = 0x0;
	memory[0xFF1E] = 0x0;
	memory[0xFF20] = 0x0;
	memory[0xFF21] = 0x0;
	memory[0xFF22] = 0x0;
	memory[0xFF23] = 0x0;
	memory[0xFF24] = 0x0;
	memory[0xFF25] = 0x0;
	memory[0xFF26] = 0x0;
	memory[0xFF30] = 0x0;
	memory[0xFF31] = 0x0;
	memory[0xFF32] = 0x0;
	memory[0xFF33] = 0x0;
	memory[0xFF34] = 0x0;
	memory[0xFF35] = 0x0;
	memory[0xFF36] = 0x0;
	memory[0xFF37] = 0x0;
	memory[0xFF38] = 0x0;
	memory[0xFF39] = 0x0;
	memory[0xFF3A] = 0x0;
	memory[0xFF3B] = 0x0;
	memory[0xFF3C] = 0x0;
	memory[0xFF3D] = 0x0;
	memory[0xFF3E] = 0x0;
	memory[0xFF3F] = 0x0;

	memreg_LCDC() = 0x80;
	memreg_STAT() = 0x3;
	memreg_SCY() = 0x0;
	memreg_SCX() = 0x0;
	memreg_LY() = LCD_START_LINE; // LDC Y
	memreg_LYC() = 0x0;

	memreg_DMA() = 0x0;
	memreg_BGP() = 0x0;
	memreg_OBP0() = 0x0;
	memreg_OBP1() = 0x0;
	memreg_WY() = 0x0;
	memreg_WX() = 0x0;

	memreg_IE() = 0x0;

	// For sanity's sake start OAM at 0
	memset(&memory[0xFE00], 0, 0xA0);

	// Start all tilemaps "dirty"
	tileChanged = std::vector<bool>(0x180, true);
}


std::string GB_CPU::bits(unsigned char byt) const
{
	std::stringstream msg;
	std::bitset<8> bits(byt);
	msg << bits;
	return msg.str();
}


