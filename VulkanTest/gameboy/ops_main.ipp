//////////////////////////////////////////////////////////
// This file contains the implementation for the main
// opcode page. It is meant to be included inside the cpu.cpp
// file (as an implementation file).
//////////////////////////////////////////////////////////



bool GB_CPU::runOpcode(unsigned char operation, const uint16_t& pc, uint16_t& nxtPC, size_t& incr, unsigned char& opSize)
{
	// Temporaries for the upcoming switch statement.
	unsigned char cbCode = 0; //CB code we're running
	uint16_t tmp2 = 0;  // General stuff

	// NOTE: This is a bit misleading, but it's true; we're loading from memory, NOT the cart (at all times).
	const std::vector<unsigned char>& program = memory;

	switch (operation) {
		
	// 0x00..0x0F
	case 0x00: // NOOP
		break;

	case 0x01: // LD BC, nn
		DO_LOAD_16(reg_B(), reg_C(), program[pc + 2], program[pc + 1]);
		break;

	case 0x02: // LD (BC), A
		DO_CHECKED_LOAD_8(reg_BC(), reg_A());
		break;

	case 0x03: // INC BC
		DO_INC_16(reg_BC());
		break;

	case 0x04: // INC B
		DO_INC_8(reg_B(), reg_F());
		break;

	case 0x05: // DEC B
		DO_DEC_8(reg_B(), reg_F());
		break;

	case 0x06: // LD B, n
		DO_LOAD_8(reg_B(), program[pc + 1]);
		break;

	case 0x07: // RLC A
		DO_RLC_8(reg_A(), reg_F());
		break;

	case 0x08: // LD (nn), SP
		DO_CHECKED_LOAD_8(program[pc + 1], reg_P());
		DO_CHECKED_LOAD_8(program[pc + 2], reg_S());
		break;

	case 0x09: // ADD HL, BC
		DO_ADD_16U(reg_HL(), reg_BC(), reg_F());
		break;

	case 0x0A: // LD A, (BC)
		DO_LOAD_8(reg_A(), memory[reg_BC()]);
		break;

	case 0x0B: // DEC BC
		DO_DEC_16(reg_BC());
		break;

	case 0x0C: // INC C
		DO_INC_8(reg_C(), reg_F());
		break;

	case 0x0D: // DEC C
		DO_DEC_8(reg_C(), reg_F());
		break;

	case 0x0E: // LD C, n
		DO_LOAD_8(reg_C(), program[pc + 1]);
		break;

	case 0x0F: // RRC A
		DO_RRC_8(reg_A(), reg_F());
		break;

	// 0x10..0x1F
	case 0x10: // STOP
		// TEMP
		std::cout << "ERROR: STOP NOT IMPLEMENTED" << std::endl; return false;
		break;
		// END TEMP

	case 0x11: // LD DE, nn
		DO_LOAD_16(reg_D(), reg_E(), program[pc + 2], program[pc + 1]);
		break;

	case 0x12: // LD (DE), A
		DO_CHECKED_LOAD_8(reg_DE(), reg_A());
		break;

	case 0x13: // INC DE
		DO_INC_16(reg_DE());
		break;

	case 0x14: // INC D
		DO_INC_8(reg_D(), reg_F());
		break;

	case 0x15: // DEC D
		DO_DEC_8(reg_D(), reg_F());
		break;

	case 0x16: // LD D, n
		DO_LOAD_8(reg_D(), program[pc + 1]);
		break;

	case 0x17: // RL A
		DO_RL_8(reg_A(), reg_F());
		break;

	case 0x18: // JR n
		nxtPC += SIGNED_8(program[pc + 1]);
		if (traceProgramFlowPoint == nxtPC) { logTrace("via 0x18 : JR n"); }
		break;

	case 0x19: // ADD HL, DE
		DO_ADD_16U(reg_HL(), reg_DE(), reg_F());
		break;

	case 0x1A: // LD A, (DE)
		DO_LOAD_8(reg_A(), memory[reg_DE()]);
		break;

	case 0x1B: // DEC DE
		DO_DEC_16(reg_DE());
		break;

	case 0x1C: // INC E
		DO_INC_8(reg_E(), reg_F());
		break;

	case 0x1D: // DEC E
		DO_DEC_8(reg_E(), reg_F());
		break;

	case 0x1E: // LD E, n
		DO_LOAD_8(reg_E(), program[pc + 1]);
		break;

	case 0x1F: // RR A
		DO_RR_8(reg_A(), reg_F());
		break;

	// 0x20..0x2F
	case 0x20: // JR NZ, n
		if (!flag_z(reg_F())) {
			nxtPC += SIGNED_8(program[pc + 1]);
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0x20 : JR NZ, n"); }
		}
		break;

	case 0x21: // LD HL, nn
		DO_LOAD_16(reg_H(), reg_L(), program[pc + 2], program[pc + 1]);
		break;

	case 0x22: // LD (HL+), A 
		DO_CHECKED_LOAD_8(reg_HL(), reg_A());
		DO_INC_16(reg_HL()); // lol, we could just fall through
		break;

	case 0x23: // INC HL 
		DO_INC_16(reg_HL());
		break;

	case 0x24: // INC H
		DO_INC_8(reg_H(), reg_F());
		break;

	case 0x25: // DEC H
		DO_DEC_8(reg_H(), reg_F());
		break;

	case 0x26: // LD H, n
		DO_LOAD_8(reg_H(), program[pc + 1]);
		break;

	case 0x27: // DAA
		DO_DAA_8(reg_A(), reg_F());
		break;

	case 0x28: // JR Z, n
		if (flag_z(reg_F())) {
			nxtPC += SIGNED_8(program[pc + 1]);
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0x28 : JR Z, n"); }
		}
		break;

	case 0x29: // ADD HL, HL
		DO_ADD_16U(reg_HL(), reg_HL(), reg_F());
		break;

	case 0x2A: // LD A, (HL+)
		DO_LOAD_8(reg_A(), memory[reg_HL()]);
		DO_INC_16(reg_HL());
		break;

	case 0x2B: // DEC HL
		DO_DEC_16(reg_HL());
		break;

	case 0x2C: // INC L
		DO_INC_8(reg_L(), reg_F());
		break;

	case 0x2D: // DEC L
		DO_DEC_8(reg_L(), reg_F());
		break;

	case 0x2E: // LD L, n
		DO_LOAD_8(reg_L(), program[pc + 1]);
		break;

	case 0x2F: // CPL
		DO_CPL_8(reg_A(), reg_F());
		break;

	// 0x30..0x3F
	case 0x30: // JR NC, n
		if (!flag_c(reg_F())) {
			nxtPC += SIGNED_8(program[pc + 1]);
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0x30 : JR NC, n"); }
		}
		break;

	case 0x31: // LD SP, nn
		DO_LOAD_16(reg_S(), reg_P(), program[pc + 2], program[pc + 1]);
		break;

	case 0x32: // LD (HL-), A
		DO_CHECKED_LOAD_8(reg_HL(), reg_A());
		DO_DEC_16(reg_HL());
		break;

	case 0x33: // INC SP
		DO_INC_16(reg_SP());
		break;

	case 0x34: // INC (HL)
		DO_INC_8(memory[reg_HL()], reg_F());
		break;

	case 0x35: // DEC (HL)
		DO_DEC_8(memory[reg_HL()], reg_F());
		break;

	case 0x36: // LD (HL), n
		DO_CHECKED_LOAD_8(reg_HL(), program[pc + 1]);
		break;

	case 0x37: // SCF
		set_flag_c(reg_F(), 1);
		set_flag_n(reg_F(), 0);
		set_flag_h(reg_F(), 0);
		break;

	case 0x38: // JR C, n
		if (flag_c(reg_F())) {
			nxtPC += SIGNED_8(program[pc + 1]);
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0x38 : JR C, n"); }
		}
		break;

	case 0x39: // ADD HL, SP
		DO_ADD_16U(reg_HL(), reg_SP(), reg_F());
		break;

	case 0x3A: // LD A, (HL-)
		DO_LOAD_8(reg_A(), memory[reg_HL()]);
		DO_DEC_16(reg_HL());
		break;

	case 0x3B: // DEC SP
		DO_DEC_16(reg_SP());
		break;

	case 0x3C: // INC A
		DO_INC_8(reg_A(), reg_F());
		break;

	case 0x3D: // DEC A
		DO_DEC_8(reg_A(), reg_F());
		break;

	case 0x3E: // LD A, n
		DO_LOAD_8(reg_A(), program[pc + 1]);
		break;

	case 0x3F: // CCF
		set_flag_c(reg_F(), !flag_c(reg_F()));
		set_flag_n(reg_F(), 0);
		set_flag_h(reg_F(), 0);
		break;

	// 0x40..0x4F
	case 0x40: // LD B, B
		DO_LOAD_8(reg_B(), reg_B());
		break;

	case 0x41: // LD B, C
		DO_LOAD_8(reg_B(), reg_C());
		break;

	case 0x42: // LD B, D
		DO_LOAD_8(reg_B(), reg_D());
		break;

	case 0x43: // LD B, E
		DO_LOAD_8(reg_B(), reg_E());
		break;

	case 0x44: // LD B, H
		DO_LOAD_8(reg_B(), reg_H());
		break;

	case 0x45: // LD B, L
		DO_LOAD_8(reg_B(), reg_L());
		break;

	case 0x46: // LD B, (HL)
		DO_LOAD_8(reg_B(), memory[reg_HL()]);
		break;

	case 0x47: // LD B, A
		DO_LOAD_8(reg_B(), reg_A());
		break;

	case 0x48: // LD C, B
		DO_LOAD_8(reg_C(), reg_B());
		break;

	case 0x49: // LD C, C
		DO_LOAD_8(reg_C(), reg_C());
		break;

	case 0x4A: // LD C, D
		DO_LOAD_8(reg_C(), reg_D());
		break;

	case 0x4B: // LD C, E
		DO_LOAD_8(reg_C(), reg_E());
		break;

	case 0x4C: // LD C, H
		DO_LOAD_8(reg_C(), reg_H());
		break;

	case 0x4D: // LD C, L
		DO_LOAD_8(reg_C(), reg_L());
		break;

	case 0x4E: // LD C, (HL)
		DO_LOAD_8(reg_C(), memory[reg_HL()]);
		break;

	case 0x4F: // LD C, A
		DO_LOAD_8(reg_C(), reg_A());
		break;

	// 0x50..0x5F
	case 0x50: // LD D, B
		DO_LOAD_8(reg_D(), reg_B());
		break;

	case 0x51: // LD D, C
		DO_LOAD_8(reg_D(), reg_C());
		break;

	case 0x52: // LD D, D
		DO_LOAD_8(reg_D(), reg_D());
		break;

	case 0x53: // LD D, E
		DO_LOAD_8(reg_D(), reg_E());
		break;

	case 0x54: // LD D, H
		DO_LOAD_8(reg_D(), reg_H());
		break;

	case 0x55: // LD D, L
		DO_LOAD_8(reg_D(), reg_L());
		break;

	case 0x56: // LD D, (HL)
		DO_LOAD_8(reg_D(), memory[reg_HL()]);
		break;

	case 0x57: // LD D, A
		DO_LOAD_8(reg_D(), reg_A());
		break;

	case 0x58: // LD E, B
		DO_LOAD_8(reg_E(), reg_B());
		break;

	case 0x59: // LD E, C
		DO_LOAD_8(reg_E(), reg_C());
		break;

	case 0x5A: // LD E, D
		DO_LOAD_8(reg_E(), reg_D());
		break;

	case 0x5B: // LD E, E
		DO_LOAD_8(reg_E(), reg_E());
		break;

	case 0x5C: // LD E, H
		DO_LOAD_8(reg_E(), reg_H());
		break;

	case 0x5D: // LD E, L
		DO_LOAD_8(reg_E(), reg_L());
		break;

	case 0x5E: // LD E, (HL)
		DO_LOAD_8(reg_E(), memory[reg_HL()]);
		break;

	case 0x5F: // LD E, A
		DO_LOAD_8(reg_E(), reg_A());
		break;

	// 0x60..0x6F
	case 0x60: // LD H, B
		DO_LOAD_8(reg_H(), reg_B());
		break;

	case 0x61: // LD H, C
		DO_LOAD_8(reg_H(), reg_C());
		break;

	case 0x62: // LD H, D
		DO_LOAD_8(reg_H(), reg_D());
		break;

	case 0x63: // LD H, E
		DO_LOAD_8(reg_H(), reg_E());
		break;

	case 0x64: // LD H, H
		DO_LOAD_8(reg_H(), reg_H());
		break;

	case 0x65: // LD H, L
		DO_LOAD_8(reg_H(), reg_L());
		break;

	case 0x66: // LD H, (HL)
		DO_LOAD_8(reg_H(), memory[reg_HL()]);
		break;

	case 0x67: // LD H, A
		DO_LOAD_8(reg_H(), reg_A());
		break;

	case 0x68: // LD L, B
		DO_LOAD_8(reg_L(), reg_B());
		break;

	case 0x69: // LD L, C
		DO_LOAD_8(reg_L(), reg_C());
		break;

	case 0x6A: // LD L, D
		DO_LOAD_8(reg_L(), reg_D());
		break;

	case 0x6B: // LD L, E
		DO_LOAD_8(reg_L(), reg_E());
		break;

	case 0x6C: // LD L, H
		DO_LOAD_8(reg_L(), reg_H());
		break;

	case 0x6D: // LD L, L
		DO_LOAD_8(reg_L(), reg_L());
		break;

	case 0x6E: // LD L, (HL)
		DO_LOAD_8(reg_L(), memory[reg_HL()]);
		break;

	case 0x6F: // LD L, A
		DO_LOAD_8(reg_L(), reg_A());
		break;

	// 0x70..0x7F
	case 0x70: // LD (HL), B
		DO_CHECKED_LOAD_8(reg_HL(), reg_B());
		break;

	case 0x71: // LD (HL), C
		DO_CHECKED_LOAD_8(reg_HL(), reg_C());
		break;

	case 0x72: // LD (HL), D
		DO_CHECKED_LOAD_8(reg_HL(), reg_D());
		break;

	case 0x73: // LD (HL), E
		DO_CHECKED_LOAD_8(reg_HL(), reg_E());
		break;

	case 0x74: // LD (HL), H
		DO_CHECKED_LOAD_8(reg_HL(), reg_H());
		break;

	case 0x75: // LD (HL), L
		DO_CHECKED_LOAD_8(reg_HL(), reg_L());
		break;

	case 0x76: // HALT
		std::cout << "TODO: HALT" << std::endl; return false;
		break;

	case 0x77: // LD (HL), A
		DO_CHECKED_LOAD_8(reg_HL(), reg_A());
		break;

	case 0x78: // LD A, B
		DO_LOAD_8(reg_A(), reg_B());
		break;

	case 0x79: // LD A, C
		DO_LOAD_8(reg_A(), reg_C());
		break;

	case 0x7A: // LD A, D
		DO_LOAD_8(reg_A(), reg_D());
		break;

	case 0x7B: // LD A, E
		DO_LOAD_8(reg_A(), reg_E());
		break;

	case 0x7C: // LD A, H
		DO_LOAD_8(reg_A(), reg_H());
		break;

	case 0x7D: // LD A, L
		DO_LOAD_8(reg_A(), reg_L());
		break;

	case 0x7E: // LD A, (HL)
		DO_LOAD_8(reg_A(), memory[reg_HL()]);
		break;

	case 0x7F: // LD A, A
		DO_LOAD_8(reg_A(), reg_A());
		break;


	// 0x80..0x8F
	case 0x80: // ADD A,B
		DO_ADD_8U(reg_A(), reg_B(), reg_F());
		break;

	case 0x81: // ADD A,C
		DO_ADD_8U(reg_A(), reg_C(), reg_F());
		break;

	case 0x82: // ADD A,D
		DO_ADD_8U(reg_A(), reg_D(), reg_F());
		break;

	case 0x83: // ADD A,E
		DO_ADD_8U(reg_A(), reg_E(), reg_F());
		break;

	case 0x84: // ADD A,H
		DO_ADD_8U(reg_A(), reg_H(), reg_F());
		break;

	case 0x85: // ADD A,L
		DO_ADD_8U(reg_A(), reg_L(), reg_F());
		break;

	case 0x86: // ADD A,(HL)
		DO_ADD_8U(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0x87: // ADD A,A
		DO_ADD_8U(reg_A(), reg_A(), reg_F());
		break;

	case 0x88: // ADC A,B
		DO_ADC_8U(reg_A(), reg_B(), reg_F());
		break;

	case 0x89: // ADC A,C
		DO_ADC_8U(reg_A(), reg_C(), reg_F());
		break;

	case 0x8A: // ADC A,D
		DO_ADC_8U(reg_A(), reg_D(), reg_F());
		break;

	case 0x8B: // ADC A,E
		DO_ADC_8U(reg_A(), reg_E(), reg_F());
		break;

	case 0x8C: // ADC A,H
		DO_ADC_8U(reg_A(), reg_H(), reg_F());
		break;

	case 0x8D: // ADC A,L
		DO_ADC_8U(reg_A(), reg_L(), reg_F());
		break;

	case 0x8E: // ADC A,(HL)
		DO_ADC_8U(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0x8F: // ADC A,A
		DO_ADC_8U(reg_A(), reg_A(), reg_F());
		break;

	// 0x90..0x9F
	case 0x90: // SUB A,B
		DO_SUB_8U(reg_A(), reg_B(), reg_F());
		break;

	case 0x91: // SUB A,C
		DO_SUB_8U(reg_A(), reg_C(), reg_F());
		break;

	case 0x92: // SUB A,D
		DO_SUB_8U(reg_A(), reg_D(), reg_F());
		break;

	case 0x93: // SUB A,E
		DO_SUB_8U(reg_A(), reg_E(), reg_F());
		break;

	case 0x94: // SUB A,H
		DO_SUB_8U(reg_A(), reg_H(), reg_F());
		break;

	case 0x95: // SUB A,L
		DO_SUB_8U(reg_A(), reg_L(), reg_F());
		break;

	case 0x96: // SUB A,(HL)
		DO_SUB_8U(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0x97: // SUB A,A
		DO_SUB_8U(reg_A(), reg_A(), reg_F());
		break;

	case 0x98: // SBC A,B
		DO_SBC_8U(reg_A(), reg_B(), reg_F());
		break;

	case 0x99: // SBC A,C
		DO_SBC_8U(reg_A(), reg_C(), reg_F());
		break;

	case 0x9A: // SBC A,D
		DO_SBC_8U(reg_A(), reg_D(), reg_F());
		break;

	case 0x9B: // SBC A,E
		DO_SBC_8U(reg_A(), reg_E(), reg_F());
		break;

	case 0x9C: // SBC A,H
		DO_SBC_8U(reg_A(), reg_H(), reg_F());
		break;

	case 0x9D: // SBC A,L
		DO_SBC_8U(reg_A(), reg_L(), reg_F());
		break;

	case 0x9E: // SBC A,(HL)
		DO_SBC_8U(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0x9F: // SBC A,A
		DO_SBC_8U(reg_A(), reg_A(), reg_F());
		break;

	// 0xA0..0xAF
	case 0xA0: // AND A,B
		DO_AND_8(reg_A(), reg_B(), reg_F());
		break;

	case 0xA1: // AND A,C
		DO_AND_8(reg_A(), reg_C(), reg_F());
		break;

	case 0xA2: // AND A,D
		DO_AND_8(reg_A(), reg_D(), reg_F());
		break;

	case 0xA3: // AND A,E
		DO_AND_8(reg_A(), reg_E(), reg_F());
		break;

	case 0xA4: // AND A,H
		DO_AND_8(reg_A(), reg_H(), reg_F());
		break;

	case 0xA5: // AND A,L
		DO_AND_8(reg_A(), reg_L(), reg_F());
		break;

	case 0xA6: // AND A,(HL)
		DO_AND_8(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0xA7: // AND A,A
		DO_AND_8(reg_A(), reg_A(), reg_F());
		break;

	case 0xA8: // XOR A,B
		DO_XOR_8(reg_A(), reg_B(), reg_F());
		break;

	case 0xA9: // XOR A,C
		DO_XOR_8(reg_A(), reg_C(), reg_F());
		break;

	case 0xAA: // XOR A,D
		DO_XOR_8(reg_A(), reg_D(), reg_F());
		break;

	case 0xAB: // XOR A,E
		DO_XOR_8(reg_A(), reg_E(), reg_F());
		break;

	case 0xAC: // XOR A,H
		DO_XOR_8(reg_A(), reg_H(), reg_F());
		break;

	case 0xAD: // XOR A,L
		DO_XOR_8(reg_A(), reg_L(), reg_F());
		break;

	case 0xAE: // XOR A,(HL)
		DO_XOR_8(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0xAF: // XOR A,A
		DO_XOR_8(reg_A(), reg_A(), reg_F());
		break;

	// 0xB0..0xBF
	case 0xB0: // OR A,B
		DO_OR_8(reg_A(), reg_B(), reg_F());
		break;

	case 0xB1: // OR A,C
		DO_OR_8(reg_A(), reg_C(), reg_F());
		break;

	case 0xB2: // OR A,D
		DO_OR_8(reg_A(), reg_D(), reg_F());
		break;

	case 0xB3: // OR A,E
		DO_OR_8(reg_A(), reg_E(), reg_F());
		break;

	case 0xB4: // OR A,H
		DO_OR_8(reg_A(), reg_H(), reg_F());
		break;

	case 0xB5: // OR A,L
		DO_OR_8(reg_A(), reg_L(), reg_F());
		break;

	case 0xB6: // OR A,(HL)
		DO_OR_8(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0xB7: // OR A,A
		DO_OR_8(reg_A(), reg_A(), reg_F());
		break;

	case 0xB8: // CP A,B
		DO_CP_8(reg_A(), reg_B(), reg_F());
		break;

	case 0xB9: // CP A,C
		DO_CP_8(reg_A(), reg_C(), reg_F());
		break;

	case 0xBA: // CP A,D
		DO_CP_8(reg_A(), reg_D(), reg_F());
		break;

	case 0xBB: // CP A,E
		DO_CP_8(reg_A(), reg_E(), reg_F());
		break;

	case 0xBC: // CP A,H
		DO_CP_8(reg_A(), reg_H(), reg_F());
		break;

	case 0xBD: // CP A,L
		DO_CP_8(reg_A(), reg_L(), reg_F());
		break;

	case 0xBE: // CP A,(HL)
		DO_CP_8(reg_A(), memory[reg_HL()], reg_F());
		break;

	case 0xBF: // CP A,A
		DO_CP_8(reg_A(), reg_A(), reg_F());
		break;

	// 0xC0..0xCF
	case 0xC0: // RET NZ
		if (!flag_z(reg_F())) {
			nxtPC = POP_STACK_16(reg_SP());
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xC0 : RET NZ"); }
			incr = 20; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xC1: // POP BC
		reg_BC() = POP_STACK_16(reg_SP());
		break;

	case 0xC2: // JP NZ, nn
		if (!flag_z(reg_F())) {
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xC2 : JP NZ, nn"); }
			incr = 16; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xC3: // JP nn
		nxtPC = program[pc + 2] << 8 | program[pc + 1];
		if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xC3 : JP nn"); }
		incr = 16; // Seems to happen only if the jump succeeds
		break;

	case 0xC4: // CALL NZ, nn
		if (!flag_z(reg_F())) {
			PUSH_STACK_16(reg_SP(), nxtPC);
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xC4 : CALL NZ, nn"); }
			incr = 24; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xC5: // PUSH BC
		PUSH_STACK_16(reg_SP(), reg_BC());
		break;

	case 0xC6: // ADD A, n
		DO_ADD_8U(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xC7: // RST 00H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0000;
		break;

	case 0xC8: // RET Z
		if (flag_z(reg_F())) {
			nxtPC = POP_STACK_16(reg_SP());
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xC8 : RET Z"); }
			incr = 20; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xC9: // RET
		nxtPC = POP_STACK_16(reg_SP());
		if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xC9 : RET"); }
		break;

	case 0xCA: // JP Z, nn
		if (flag_z(reg_F())) {
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xCA : JP Z, nn"); }
			incr = 16; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xCB: // Page CB Lookup
			   // Pull an extra operand off; use it to look up the CB code op and cycles
		cbCode = program[pc + 1];
		runCBPageCode(cbCode);
		incr += op_CB_ops_cycles[cbCode * 2 + 1];
		opSize += op_CB_ops_cycles[cbCode * 2] + 1;
		nxtPC += op_CB_ops_cycles[cbCode * 2] + 1;
		break;

	case 0xCC: // CALL Z, nn
		if (flag_z(reg_F())) {
			PUSH_STACK_16(reg_SP(), nxtPC);
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xCC : CALL Z, nn"); }
			incr = 24; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xCD: // CALL  nn
			   // Save our current location to the stack, jump to the new location
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = program[pc + 2] << 8 | program[pc + 1];
		if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xCD : CALL nn"); }
		incr = 24; // Seems to happen only if the jump succeeds
		break;

	case 0xCE: // ADC A, n
		DO_ADC_8U(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xCF: // RST 08H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0008;
		break;

	// 0xD0..0xDF
	case 0xD0: // RET NC
		if (!flag_c(reg_F())) {
			nxtPC = POP_STACK_16(reg_SP());
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xD0 : RET NC"); }
			incr = 20; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xD1: // POP DE
		reg_DE() = POP_STACK_16(reg_SP());
		break;

	case 0xD2: // JP NC, nn
		if (!flag_c(reg_F())) {
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xD2 : JP NC, nn"); }
			incr = 16; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xD3: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xD4: // CALL NC, nn
		if (!flag_c(reg_F())) {
			PUSH_STACK_16(reg_SP(), nxtPC);
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xD4 : CALL NC, nn"); }
			incr = 24; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xD5: // PUSH DE
		PUSH_STACK_16(reg_SP(), reg_DE());
		break;

	case 0xD6: // SUB A, n
		DO_SUB_8U(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xD7: // RST 10H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0010;
		break;

	case 0xD8: // RET C
		if (flag_c(reg_F())) {
			nxtPC = POP_STACK_16(reg_SP());
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xD8 : RET C"); }
			incr = 20; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xD9: // RETI
		nxtPC = POP_STACK_16(reg_SP());
		if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xD9 : RETI"); }
		ime = true;
		break;

	case 0xDA: // JP C, nn
		if (flag_c(reg_F())) {
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xDA : JP C, nn"); }
			incr = 16; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xDB: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xDC: // CALL C, nn
		if (flag_c(reg_F())) {
			PUSH_STACK_16(reg_SP(), nxtPC);
			nxtPC = program[pc + 2] << 8 | program[pc + 1];
			if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xDC : CALL C, nn"); }
			incr = 24; // Seems to happen only if the jump succeeds
		}
		break;

	case 0xDD: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xDE: // SBC A,n
		DO_SBC_8U(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xDF: // RST 18H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0018;
		break;

	// 0xE0..0xEF
	case 0xE0: // LD (FF00+n), A
		DO_CHECKED_LOAD_8(0xFF00 | program[pc + 1], reg_A());
		break;

	case 0xE1: // POP HL
		reg_HL() = POP_STACK_16(reg_SP());
		break;

	case 0xE2: // LD (FF00+C), A
		DO_CHECKED_LOAD_8(0xFF00 | reg_C(), reg_A());
		break;

	case 0xE3: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xE4: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xE5: // PUSH HL
		PUSH_STACK_16(reg_SP(), reg_HL());
		break;

	case 0xE6: // AND A, n
		DO_AND_8(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xE7: // RST 20H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0020;
		break;

	case 0xE8: // ADD SP, n
		DO_ADD_16U(reg_SP(), program[pc + 1], reg_F());
		break;

	case 0xE9: // JP (HL)
		nxtPC = reg_HL();
		if (traceProgramFlowPoint == nxtPC) { logTrace("via 0xE9 : JP (HL)"); }
		break;

	case 0xEA: // LD (nn), A
		DO_CHECKED_LOAD_8(program[pc + 2] << 8 | program[pc + 1], reg_A());
		break;

	case 0xEB: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xEC: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xED: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xEE: // XOR A,n
		DO_XOR_8(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xEF: // RST 28H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0028;
		break;

	// 0xF0..0xFF
	case 0xF0: // LD A, (FF00+n)
		DO_LOAD_8(reg_A(), memory[0xFF00 | program[pc + 1]]);
		break;

	case 0xF1: // POP AF
		reg_AF() = POP_STACK_16(reg_SP());
		break;

	case 0xF2: // LD A, (FF00+C)
		DO_LOAD_8(reg_A(), memory[0xFF00 | reg_C()]);
		break;

	case 0xF3: // DI
		// TODO: This actually happens 1 cycle later, but games plan for it, so...
		ime = false;
		break;

	case 0xF4: // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xF5: // PUSH AF
		PUSH_STACK_16(reg_SP(), reg_AF());
		break;

	case 0xF6: // OR A,n
		DO_OR_8(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xF7: // RST 30H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0030;
		break;

	case 0xF8: // LD HL, SP+n
		tmp2 = reg_SP() + SIGNED_8(program[pc + 1]);
		DO_LOAD_16(reg_H(), reg_L(), tmp2 & 0xFF, (tmp2 >> 8) & 0xFF);
		break;

	case 0xF9: // LD SP, HL
		DO_LOAD_16(reg_S(), reg_P(), reg_H(), reg_L());
		break;

	case 0xFA: // LD A, (nn)
		DO_LOAD_8(reg_A(), memory[program[pc + 2] << 8 | program[pc + 1]]);
		break;

	case 0xFB: // EI
		// TODO: Same timing issue
		ime = true;
		break;

	case 0xFC: // // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xFD: // // <INVALID>
		incr = 4;
		break; // treat as no-op

	case 0xFE: // CP A,n
		DO_CP_8(reg_A(), program[pc + 1], reg_F());
		break;

	case 0xFF: // RST 38H
		PUSH_STACK_16(reg_SP(), nxtPC);
		nxtPC = 0x0038;
		break;

	default:
		// Should never happen
		std::cout << "Skipping unknown opcode: " << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << (int(operation) & 0xFF) << std::endl;
		throw std::runtime_error("Skipping unknown opcode");
	}

	return true;
}

