// Grumble, grumble, vs...
#ifndef _ITERATOR_DEBUG_LEVEL
#define _ITERATOR_DEBUG_LEVEL 0
#endif

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <map>
#include <thread>
#include <chrono>
#include <iostream>
#include <stdexcept>
#include <functional>
#include <algorithm>
#include <cstdlib>
#include <optional>
#include <vector>
#include <memory>
#include <array>
#include <fstream>
#include <string>
#include <cstring>
#include <set>

#include "gameboy/cpu.h"

// Default game name
#define DEFAULT_GAME "Tetris.gb"

// Are we running in "realtime" mode?
#define REALTIME true

// Global variable: our GB CPU/SOC
std::unique_ptr<GB_CPU> cpu;

const int WIDTH = 800;
const int HEIGHT = 600;
const int MAX_FRAMES_IN_FLIGHT = 2;

// Sample colors
struct RGB {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
};
const RGB GB_PAL_WHITE = { 0xFF, 0xFF, 0xFF };
const RGB GB_PAL_LGRAY = { 0x99, 0x99, 0x99 };
const RGB GB_PAL_DGRAY = { 0x55, 0x55, 0x55 };
const RGB GB_PAL_BLACK = { 0x00, 0x00, 0x00 };

// Easy way to say "not a tile"
#define TLDAT_NO_TILE {0, 0}

const bool enableValidationLayers =
#ifdef _DEBUG 
true;
#else
#ifdef __OPTIMIZE__
false;
#else
true;
#endif
#endif


const std::vector<const char*> validationLayers = {
	"VK_LAYER_LUNARG_standard_validation"
};

const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

struct Vertex {
	glm::vec2 pos;
	glm::vec2 texCoord;
	glm::uvec2 tileData;  // [0] = 0 for "unmanaged", 1 for "it's a tile", 2 for "it's a sprite"

	static VkVertexInputBindingDescription getBindingDescriptions() {
		VkVertexInputBindingDescription bindings = {};

		bindings.binding = 0;
		bindings.stride = sizeof(Vertex);
		bindings.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindings;
	}

	static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 3> attributes = {};

		attributes[0].binding = 0;
		attributes[0].location = 0;
		attributes[0].format = VK_FORMAT_R32G32_SFLOAT;
		attributes[0].offset = offsetof(Vertex, pos);

		attributes[1].binding = 0;
		attributes[1].location = 1;
		attributes[1].format = VK_FORMAT_R32G32_SFLOAT;
		attributes[1].offset = offsetof(Vertex, texCoord);

		attributes[2].binding = 0;
		attributes[2].location = 2;
		attributes[2].format = VK_FORMAT_R32G32_UINT;
		attributes[2].offset = offsetof(Vertex, tileData);

		return attributes;
	}

};

// We define our vertices in a world space of:
//    160 x 144 "pixels"
const float TLSZ = 8;
const float NUDGE = 0.01f;
std::vector<Vertex> vertices;
std::vector<uint16_t> indices;

// Useful organizer
struct SimpleRect {
	int x;
	int y;
	int width;
	int height;
};

const bool TMP_VDGB = false;
void get_vertices_and_indices(std::vector<Vertex>& vertices, std::vector<uint16_t>& indices) {
	vertices.clear();
	indices.clear();

	// Get the full list of tiles.
	// Each tile is a byte (either signed or unsigned, depending on the mode), but we represent it as an absolute index from 0x00 to 0x7F
	std::vector<unsigned char> bgTileMap = cpu->getBgTileMap();

	const size_t QW = 0x10; //16x24
	const size_t QH = 0x08 * 3;

	// GameBoy full background is 0x20 by 0x20, with a window 0x13 by 0x11 tiles
	size_t currIndex = 0;
	uint16_t maxIndex = 0;
	for (int tileY = 0; tileY < GB_BG_FULL_HEIGHT; tileY++) {
		for (int tileX = 0; tileX < GB_BG_FULL_WIDTH; tileX++) {
			// Prepare the x/y coords
			float x = tileX * TLSZ;
			float y = tileY * TLSZ;

			// Get the tile to use.
			// If no tiles have been defined yet, we still use *some* tile ID.
			unsigned char tileId = 0;
			if (currIndex < bgTileMap.size()) {
				tileId = bgTileMap[currIndex];
			}

			// Prepare the texture coords
			float tx = (tileId % QW)*TLSZ;
			float ty = ((tileId / QW))*TLSZ;

			if (TMP_VDGB) {
				std::cout << "TILE: " << tileX << "x" << tileY << " at " << x << "," << y << " with tiles: " << tx << "," << ty << std::endl;
				std::cout << "  coords: " << x << "," << y << std::endl;
				std::cout << "  coords: " << x + TLSZ << "," << y << std::endl;
				std::cout << "  coords: " << x + TLSZ << "," << y + TLSZ << std::endl;
				std::cout << "  coords: " << x << "," << y + TLSZ << std::endl;
			}

			// Add all four vertices. Texture coords need to be nudged back to the center of the texel, or else they'll look fuzzy.
			vertices.push_back( {{x,      y},       { NUDGE,       NUDGE        },{ 1, currIndex}, });
			vertices.push_back( {{x+TLSZ, y},       { NUDGE+TLSZ,  NUDGE        },{ 1, currIndex}, });
			vertices.push_back( {{x+TLSZ, y+TLSZ},  { NUDGE+TLSZ,  NUDGE+TLSZ },{ 1, currIndex}, });
			vertices.push_back( {{x,      y+TLSZ},  { NUDGE,       NUDGE+TLSZ },{ 1, currIndex}, });

			if (TMP_VDGB) {
				std::cout << "  total vertices so far: " << vertices.size() << std::endl;
			}

			currIndex += 1;

			// Add all six indices
			uint16_t ind = maxIndex;
			maxIndex += 4;
			indices.push_back(ind);
			indices.push_back(ind+1);
			indices.push_back(ind+2);
			indices.push_back(ind+2);
			indices.push_back(ind+3);
			indices.push_back(ind);

			if (TMP_VDGB) {
				std::cout	<< "  indices: "
							<< ind << ","
							<< ind + 1 << ","
							<< ind + 2 << ","
							<< ind + 2 << ","
							<< ind + 3 << ","
							<< ind
							<< std::endl;
				std::cout << "  total indices so far: " << indices.size() << std::endl;
			}
		}
	}

	// We also need some sprites
	auto sprites = cpu->getSprites();
	for (size_t i=0; i<40; i++) {
		float x = -999;
		float y = -999;
		int tileId = 0;

		if (i < sprites.size()) {
			const auto& spr = sprites[i];
			x = std::get<1>(spr) - 8.0f;
			y = std::get<0>(spr) - 16.0f;
			tileId = std::get<2>(spr);
		}

		// Prepare the texture coords
		float tx = (tileId % QW)*TLSZ;
		float ty = ((tileId / QW))*TLSZ;

		// Add all four vertices. Texture coords need to be nudged back to the center of the texel, or else they'll look fuzzy.
		unsigned int spriteId = static_cast<unsigned int>(i);
		vertices.push_back({ { 0,      0 },      { NUDGE,         NUDGE },{ 2, spriteId }, });
		vertices.push_back({ { TLSZ, 0 },      { NUDGE + TLSZ,  NUDGE },{ 2, spriteId }, });
		vertices.push_back({ { TLSZ, TLSZ }, { NUDGE + TLSZ,  NUDGE + TLSZ },{ 2, spriteId }, });
		vertices.push_back({ { 0,      TLSZ }, { NUDGE,         NUDGE + TLSZ },{ 2, spriteId }, });
		indices.push_back(maxIndex);
		indices.push_back(maxIndex + 1);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 3);
		indices.push_back(maxIndex);
		maxIndex += 4;
	}


	// Add the tilemap on the side
	//   128x192
	{
		float x = 64 * 7;
		float y = -64;
		vertices.push_back({ { x,      y },{ NUDGE + 0,         NUDGE + 0 }, TLDAT_NO_TILE, });
		vertices.push_back({ { x + 128, y },{ NUDGE + 0 + 128,  NUDGE + 0 }, TLDAT_NO_TILE, });
		vertices.push_back({ { x + 128, y + 192 },{ NUDGE + 0 + 128,  NUDGE + 0 + 192 }, TLDAT_NO_TILE, });
		vertices.push_back({ { x,      y + 192 },{ NUDGE + 0,         NUDGE + 0 + 192 }, TLDAT_NO_TILE, });
		indices.push_back(maxIndex);
		indices.push_back(maxIndex + 1);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 3);
		indices.push_back(maxIndex);
		maxIndex += 4;
	}


	// Add the button press graphic
	//   161x64
	float ctlX = 64 * 7 - (161 - 128) / 2;
	float ctlY = 200;
	{
		float x = ctlX;
		float y = ctlY;
		vertices.push_back({ { x,      y },{ NUDGE + 192,         NUDGE + 0 }, TLDAT_NO_TILE, });
		vertices.push_back({ { x + 161, y },{ NUDGE + 192 + 161,  NUDGE + 0 }, TLDAT_NO_TILE, });
		vertices.push_back({ { x + 161, y + 64 },{ NUDGE + 192 + 161,  NUDGE + 0 + 64 }, TLDAT_NO_TILE, });
		vertices.push_back({ { x,      y + 64 },{ NUDGE + 192,         NUDGE + 0 + 64 }, TLDAT_NO_TILE, });
		indices.push_back(maxIndex);
		indices.push_back(maxIndex + 1);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 3);
		indices.push_back(maxIndex);
		maxIndex += 4;
	}


	// Add the button press overlays themselves
	//   1st rect = 
	std::vector<SimpleRect> btns = {
		{196, 23, 14, 16}, // Left
		{226, 23, 14, 16}, // Right
		{210,  9, 16, 14}, // Up
		{210, 39, 16, 14}, // Down

		{248, 20, 22, 22}, // Select
		{271, 20, 22, 22}, // Start
		{298, 29, 22, 22}, // B
		{321, 10, 22, 22}, // A
	};
	for (size_t btnId = 0; btnId < btns.size(); btnId++) {
		const auto& btn = btns[btnId];
		float x = ctlX + btn.x - 192;
		float y = ctlY + btn.y;
		float tileX = static_cast<float>(btn.x);
		float tileY = static_cast<float>(btn.y) + 64;
		vertices.push_back({ { x,             y },             { NUDGE + tileX,              NUDGE + tileY },              { 3, btnId }, });
		vertices.push_back({ { x + btn.width, y },             { NUDGE + tileX + btn.width,  NUDGE + tileY },              { 3, btnId }, });
		vertices.push_back({ { x + btn.width, y + btn.height },{ NUDGE + tileX + btn.width,  NUDGE + tileY + btn.height }, { 3, btnId }, });
		vertices.push_back({ { x,             y + btn.height },{ NUDGE + tileX,              NUDGE + tileY + btn.height }, { 3, btnId }, });
		indices.push_back(maxIndex);
		indices.push_back(maxIndex + 1);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 2);
		indices.push_back(maxIndex + 3);
		indices.push_back(maxIndex);
		maxIndex += 4;
	}




	// TODO (also some way to disable them via the ubo)


}


struct UniformBufferObject {
	glm::vec2 win;
	glm::vec2 map;
	glm::uvec4 tiles[0x100];   // 0x100*4 tiles
	glm::ivec4 sprites[0x28];  // 40 sprites (x,y,tileId, unused)
	glm::ivec4 buttons[8];  // 8 buttons ("visible", unused, unused, unused)
};


class SampleApp {
public:
	SampleApp(std::string title) : title(title), window(nullptr), physicalDevice(VK_NULL_HANDLE), currentFrame(0), framebufferResized(false) {
	}

	void run() {
		initWindow();
		initVulkan();

		mainLoop();

		vkDeviceWaitIdle(device);
		cleanupVulkan();
		cleanupWindow();
	}

private:
	struct QueueFamilyIndices {
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;

		bool isComplete() {
			return graphicsFamily.has_value() && presentFamily.has_value();
		}
	};

	struct SwapChainSupportDetails {
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData
	) {
		std::cerr << "VALIDATION layer reports: " << pCallbackData->pMessage << std::endl;
		return VK_FALSE;
	}

	static std::vector<char> readFile(const std::string& filename) {
		std::ifstream inFile(filename, std::ios::ate | std::ios::binary);
		if (!inFile.is_open()) {
			throw std::runtime_error("Could not load file...");
		}

		size_t fileSize = static_cast<size_t>(inFile.tellg());
		std::vector<char> buffer(fileSize);

		inFile.seekg(0);
		inFile.read(buffer.data(), fileSize);
		inFile.close();

		return buffer;
	}

	void initWindow() {
		glfwInit();

		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

		window = glfwCreateWindow(WIDTH, HEIGHT, title.c_str(), nullptr, nullptr);
		glfwSetWindowUserPointer(window, this);
		glfwSetFramebufferSizeCallback(window, frameBufferResizeCallback);
	}

	static void frameBufferResizeCallback(GLFWwindow* window, int width, int height) {
		auto app = reinterpret_cast<SampleApp*>(glfwGetWindowUserPointer(window));
		app->framebufferResized = true;
	}

	std::vector<const char*> getRequiredExtensions() {
		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

		if (enableValidationLayers) {
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		return extensions;
	}

	bool checkValidationLayerSupport() {
		uint32_t layerCount = 0;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		bool res = true;
		for (const char* layerName : validationLayers) {
			bool layerFound = false;
			for (const VkLayerProperties& prop : availableLayers) {
				if (strcmp(layerName, prop.layerName) == 0) {
					layerFound = true;
					break;
				}
			}
			if (!layerFound) {
				std::cerr << "Validation layer '" << layerName << "' was not found." << std::endl;
				res = false;
			}
		}

		return res;
	}

	void createInstance() {
		if (enableValidationLayers && !checkValidationLayerSupport()) {
			throw std::runtime_error("Validation layer could not be established; missing support.");
		}

		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "Test Vulkan App";
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "No Engine";
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_1;

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;

		auto extensions = getRequiredExtensions();
		createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		createInfo.ppEnabledExtensionNames = extensions.data();

		if (enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else {
			createInfo.enabledLayerCount = 0;
		}

		for (const char* ext : extensions) {
			std::cout << "Extension: " << ext << std::endl;
		}
		

		if (VK_SUCCESS != vkCreateInstance(&createInfo, nullptr, &instance)) {
			throw std::runtime_error("Call to 'vkCreateInstance' failed; no instance was created.");
		}

	}

	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pCallback) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func) {
			return func(instance, pCreateInfo, pAllocator, pCallback);
		}
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}

	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator) {
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (func) {
			func(instance, callback, pAllocator);
		}
	}

	void setupDebugCallback() {
		if (!enableValidationLayers) { return; }

		VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		createInfo.pfnUserCallback = debugCallback;
		createInfo.pUserData = nullptr;

		if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &callback) != VK_SUCCESS) {
			throw std::runtime_error("Failed to set up debug callback.");
		}
	}

	void createSurface() {
		if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
			throw std::runtime_error("Couldn't create surface from window.");
		}
	}

	void initVulkan() {
		createInstance();
		setupDebugCallback();
		createSurface();
		pickPhysicalDevice();
		createLogicalDevice();
		createSwapChain();
		createImageViews();
		createRenderPass();
		createDescriptorSetLayout();
		createGraphicsPipeline();
		createFrameBuffers();
		createCommandPool();
		createTextureImages();
		createTextureImageView();
		createTextureSampler();
		createVertexBuffers();
		createIndexBuffers();
		createUniformBuffers();
		createDescriptorPool();
		createDescriptorSets();
		createCommandBuffers();
		createSemaphoresAndFences();
	}

	void pickPhysicalDevice() {
		uint32_t deviceCount = 0;
		vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
		if (deviceCount == 0) {
			throw std::runtime_error("Can't find physical device for vulkan.");
		}

		std::vector<VkPhysicalDevice> devices(deviceCount);
		vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

		for (VkPhysicalDevice& device : devices) {
			if (isDeviceSuitable(device)) {
				physicalDevice = device;
				break;
			}
		}

		if (physicalDevice == VK_NULL_HANDLE) {
			throw std::runtime_error("Failed to find suitable device for vulkan");
		}
	}

	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
		if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
			std::cout << "Swap Surface format: IDEAL match found" << std::endl;
			return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
		}

		for (const VkSurfaceFormatKHR& format : availableFormats) {
			if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				std::cout << "Swap Surface format: EXACT match (but non-ideal) found" << std::endl;
				return format;
			}
		}

		std::cout << "Swap Surface format: BAD match found, picking first option" << std::endl;
		return availableFormats[0];
	}

	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availableModes) {
		VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

		for (const VkPresentModeKHR& mode : availableModes) {
			if (mode == VK_PRESENT_MODE_MAILBOX_KHR) {
				std::cout << "Swap Surface present mode: BEST match found; using triple-buffering" << std::endl;
				return mode;
			}
			else if (mode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
				bestMode = mode;
			}
		}

		if (bestMode == VK_PRESENT_MODE_FIFO_KHR) {
			std::cout << "Swap Surface present mode: NO match found; using vsync" << std::endl;
		}
		else if (bestMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			std::cout << "Swap Surface present mode: NO match found; using vsync+immediate" << std::endl;
		}
		else {
			std::cout << "Swap Surface present mode: NO match found; using ???" << std::endl;
		}
		
		return bestMode;
	}


	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			std::cout << "Swap Surface extent: currentExtent is fine at: " <<capabilities.currentExtent.width <<"x" << capabilities.currentExtent.height <<std::endl;
			return capabilities.currentExtent;
		}
		else {
			int width = 0;
			int height = 0;
			glfwGetFramebufferSize(window, &width, &height);

			VkExtent2D actualExtent = {static_cast<uint32_t>(width), static_cast<uint32_t>(height) };
			actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

			std::cout << "Swap Surface extent: actualExtent had to be computed to: " << actualExtent.width <<"x" << actualExtent.height <<" via window of size: " <<WIDTH <<"x" <<HEIGHT << std::endl;
			return actualExtent;
		}
	}


	bool isDeviceSuitable(VkPhysicalDevice device) {
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties(device, &props);

		VkPhysicalDeviceFeatures features;
		vkGetPhysicalDeviceFeatures(device, &features);

		std::cout << "Device: " << props.deviceName <<" (" << props.driverVersion <<")" <<std::endl;

		bool extensionsSupported = checkDeviceExtensionsSupported(device);

		bool swapChainAdequate = false;
		if (extensionsSupported) {
			SwapChainSupportDetails details = querySwapChainSupport(device);
			swapChainAdequate = (!details.formats.empty()) && (!details.presentModes.empty());
		}

		QueueFamilyIndices indices = findQueueFamilies(device);
		return indices.isComplete() && extensionsSupported && swapChainAdequate && features.samplerAnisotropy;
	}

	bool checkDeviceExtensionsSupported(VkPhysicalDevice device) {
		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());
		for (const VkExtensionProperties& prop : availableExtensions) {
			requiredExtensions.erase(prop.extensionName);
		}

		return requiredExtensions.empty();
	}

	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
		QueueFamilyIndices indices;

		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

		int i = 0;
		for (const VkQueueFamilyProperties& queueFamily : queueFamilies) {
			if (queueFamily.queueCount > 0 && (queueFamily.queueFlags&VK_QUEUE_GRAPHICS_BIT)) {
				indices.graphicsFamily = i;
			}

			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

			if (queueFamily.queueCount > 0 && presentSupport) {
				indices.presentFamily = i;
			}

			if (indices.isComplete()) {
				break;
			}

			i++;
		}

		return indices;
	}


	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) {
		SwapChainSupportDetails details;

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

		uint32_t formatCount = 0;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
		if (formatCount > 0) {
			details.formats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
		}

		uint32_t presentCount = 0;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentCount, nullptr);
		if (presentCount > 0) {
			details.presentModes.resize(presentCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentCount, details.presentModes.data());
		}

		return details;
	}


	void createLogicalDevice() {
		QueueFamilyIndices indices = findQueueFamilies(physicalDevice);

		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		float queuePriority = 1;
		for (uint32_t queueFamily : uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo queueCreateInfo = {};
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = queueFamily;
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;

			queueCreateInfos.push_back(queueCreateInfo);
		}

		VkPhysicalDeviceFeatures deviceFeatures = {};
		deviceFeatures.samplerAnisotropy = VK_TRUE;

		VkDeviceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

		createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
		createInfo.pQueueCreateInfos = queueCreateInfos.data();

		createInfo.pEnabledFeatures = &deviceFeatures;

		createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
		createInfo.ppEnabledExtensionNames = deviceExtensions.data();

		if (enableValidationLayers) {
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else {
			createInfo.enabledLayerCount = 0;
		}

		if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create vulkan logical device.");
		}

		vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);
		vkGetDeviceQueue(device, indices.presentFamily.value(), 0, &presentQueue);
	}

	void createSwapChain() {
		SwapChainSupportDetails details = querySwapChainSupport(physicalDevice);

		VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(details.formats);
		VkPresentModeKHR presentMode = chooseSwapPresentMode(details.presentModes);
		VkExtent2D extent = chooseSwapExtent(details.capabilities);

		uint32_t imageCount = details.capabilities.minImageCount + 1;
		if (details.capabilities.maxImageCount > 0 && imageCount > details.capabilities.maxImageCount) {
			std::cout << "Warning: ImageCount forced to max of: " << details.capabilities.maxImageCount << " instead of the expected: " << imageCount << std::endl;
			imageCount = details.capabilities.maxImageCount;
		}
		else {
			std::cout << "ImageCount set to: " << imageCount << std::endl;
		}

		VkSwapchainCreateInfoKHR createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = surface;

		createInfo.minImageCount = imageCount;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = extent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		
		QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
		uint32_t queueFamilyIndices[] = {indices.graphicsFamily.value(), indices.presentFamily.value()};
		if (indices.graphicsFamily != indices.presentFamily) {
			std::cout << "Image Sharing Mode: Concurrent" << std::endl;
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		else {
			std::cout << "Image Sharing Mode: Exclusive" << std::endl;
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			createInfo.queueFamilyIndexCount = 0;
			createInfo.pQueueFamilyIndices = nullptr;
		}

		createInfo.preTransform = details.capabilities.currentTransform;
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		createInfo.presentMode = presentMode;
		createInfo.clipped = VK_TRUE;

		createInfo.oldSwapchain = VK_NULL_HANDLE;

		if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create swap chain");
		}

		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
		swapChainImages.resize(imageCount);
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());

		swapChainImageFormat = surfaceFormat.format;
		swapChainExtent = extent;
		
	}
	
	void cleanupSwapChain() {
		for (VkFramebuffer& framebuffer : swapChainFrameBuffers) {
			vkDestroyFramebuffer(device, framebuffer, nullptr);
		}
		vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());

		vkDestroyPipeline(device, graphicsPipeline, nullptr);
		vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
		vkDestroyRenderPass(device, renderPass, nullptr);

		for (VkImageView& imgView : swapChainImageViews) {
			vkDestroyImageView(device, imgView, nullptr);
		}
		vkDestroySwapchainKHR(device, swapChain, nullptr);
	}

	void recreateSwapChain() {
		int width = 0;
		int height = 0;
		while (width == 0 || height == 0) {
			glfwGetFramebufferSize(window, &width, &height);
			glfwWaitEvents();
		}

		vkDeviceWaitIdle(device);

		cleanupSwapChain();

		createSwapChain();
		createImageViews();
		createRenderPass();
		createGraphicsPipeline();
		createFrameBuffers();
		createCommandBuffers();
	}

	void createImageViews() {
		swapChainImageViews.resize(swapChainImages.size());

		for (size_t i = 0; i < swapChainImages.size(); i++) {
			swapChainImageViews[i] = createImageView(swapChainImages[i], swapChainImageFormat);
		}
	}

	void createRenderPass() {
		VkAttachmentDescription colorAttachment = {};
		colorAttachment.format = swapChainImageFormat;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &colorAttachmentRef;

		VkSubpassDependency dep = {};
		dep.srcSubpass = VK_SUBPASS_EXTERNAL;
		dep.dstSubpass = 0;
		dep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dep.srcAccessMask = 0;
		dep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;


		VkRenderPassCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		createInfo.attachmentCount = 1;
		createInfo.pAttachments = &colorAttachment;
		createInfo.subpassCount = 1;
		createInfo.pSubpasses = &subpass;

		createInfo.dependencyCount = 1;
		createInfo.pDependencies = &dep;

		if (vkCreateRenderPass(device, &createInfo, nullptr, &renderPass) != VK_SUCCESS) {
			throw std::runtime_error("Render pass creation failed.");
		}
	} 

	void createDescriptorSetLayout() {
		VkDescriptorSetLayoutBinding uboLayoutBinding = {};
		uboLayoutBinding.binding = 0;
		uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		uboLayoutBinding.descriptorCount = 1;
		uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

		VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
		samplerLayoutBinding.binding = 1;
		samplerLayoutBinding.descriptorCount = 1;
		samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		samplerLayoutBinding.pImmutableSamplers = nullptr;
		samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		std::array<VkDescriptorSetLayoutBinding, 2> bindings = { uboLayoutBinding, samplerLayoutBinding };

		VkDescriptorSetLayoutCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		createInfo.bindingCount = static_cast<uint32_t>(bindings.size());
		createInfo.pBindings = bindings.data();

		if (vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
			throw std::runtime_error("Could not create descriptor set layout.");
		}
	}

	void createGraphicsPipeline() {
		std::vector<char> vertexShaderCode = readFile("shaders/vert.spv");
		std::vector<char> fragmentShaderCode = readFile("shaders/frag.spv");

		VkShaderModule vertexShader = createShaderModule(vertexShaderCode);
		VkShaderModule fragmentShader = createShaderModule(fragmentShaderCode);

		VkPipelineShaderStageCreateInfo createInfoVertex = {};
		createInfoVertex.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		createInfoVertex.stage = VK_SHADER_STAGE_VERTEX_BIT;
		createInfoVertex.module = vertexShader;
		createInfoVertex.pName = "main";

		VkPipelineShaderStageCreateInfo createInfoFragment = {};
		createInfoFragment.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		createInfoFragment.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		createInfoFragment.module = fragmentShader;
		createInfoFragment.pName = "main";

		VkPipelineShaderStageCreateInfo shaderStages[] = { createInfoVertex, createInfoFragment };

		auto bindingDesc = Vertex::getBindingDescriptions();
		auto attributeDesc = Vertex::getAttributeDescriptions();

		VkPipelineVertexInputStateCreateInfo createInfoVertexPipe = {};
		createInfoVertexPipe.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		createInfoVertexPipe.vertexBindingDescriptionCount = 1;
		createInfoVertexPipe.pVertexBindingDescriptions = &bindingDesc;
		createInfoVertexPipe.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDesc.size());
		createInfoVertexPipe.pVertexAttributeDescriptions = attributeDesc.data();

		VkPipelineInputAssemblyStateCreateInfo createInfoAssemblyPipeline = {};
		createInfoAssemblyPipeline.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		createInfoAssemblyPipeline.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		createInfoAssemblyPipeline.primitiveRestartEnable = VK_FALSE;

		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = static_cast<float>(swapChainExtent.width);
		viewport.height = static_cast<float>(swapChainExtent.height);
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		VkRect2D scissor = {};
		scissor.offset = {0, 0};
		scissor.extent = swapChainExtent;

		VkPipelineViewportStateCreateInfo createInfoViewportState = {};
		createInfoViewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		createInfoViewportState.viewportCount = 1;
		createInfoViewportState.pViewports = &viewport;
		createInfoViewportState.scissorCount = 1;
		createInfoViewportState.pScissors = &scissor;

		VkPipelineRasterizationStateCreateInfo createInofPipelineRaster = {};
		createInofPipelineRaster.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		createInofPipelineRaster.depthClampEnable = VK_FALSE;
		createInofPipelineRaster.rasterizerDiscardEnable = VK_FALSE;
		createInofPipelineRaster.polygonMode = VK_POLYGON_MODE_FILL;
		createInofPipelineRaster.lineWidth = 1.0f;
		createInofPipelineRaster.cullMode = VK_CULL_MODE_NONE;
		createInofPipelineRaster.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		createInofPipelineRaster.depthBiasEnable = VK_FALSE;

		VkPipelineMultisampleStateCreateInfo createInfoMultisample = {};
		createInfoMultisample.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		createInfoMultisample.sampleShadingEnable = VK_FALSE;
		createInfoMultisample.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

		VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_A_BIT | VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT;
		colorBlendAttachment.blendEnable = VK_TRUE;
		colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
		colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
		colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
		colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
		colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
		colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

		VkPipelineColorBlendStateCreateInfo createInfoColorBlend = {};
		createInfoColorBlend.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		createInfoColorBlend.logicOpEnable = VK_FALSE;
		createInfoColorBlend.attachmentCount = 1;
		createInfoColorBlend.pAttachments = &colorBlendAttachment;

		VkDynamicState dynamicStates[] = {
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_LINE_WIDTH
		};

		VkPipelineDynamicStateCreateInfo createInfoDynStates = {};
		createInfoDynStates.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		createInfoDynStates.dynamicStateCount = 2;
		createInfoDynStates.pDynamicStates = dynamicStates;

		VkPipelineLayoutCreateInfo createInfoPipelineLayout = {};
		createInfoPipelineLayout.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		createInfoPipelineLayout.setLayoutCount = 1;
		createInfoPipelineLayout.pSetLayouts = &descriptorSetLayout;

		if (vkCreatePipelineLayout(device, &createInfoPipelineLayout, nullptr, &pipelineLayout) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create pipeline layout.");
		}

		VkGraphicsPipelineCreateInfo createInfoGraphics = {};
		createInfoGraphics.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		createInfoGraphics.stageCount = 2;
		createInfoGraphics.pStages = shaderStages;

		createInfoGraphics.pVertexInputState = &createInfoVertexPipe;
		createInfoGraphics.pInputAssemblyState = &createInfoAssemblyPipeline;
		createInfoGraphics.pViewportState = &createInfoViewportState;
		createInfoGraphics.pRasterizationState = &createInofPipelineRaster;
		createInfoGraphics.pMultisampleState = &createInfoMultisample;
		createInfoGraphics.pColorBlendState = &createInfoColorBlend;

		createInfoGraphics.layout = pipelineLayout;
		createInfoGraphics.renderPass = renderPass;
		createInfoGraphics.subpass = 0;

		if (vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &createInfoGraphics, nullptr, &graphicsPipeline) != VK_SUCCESS) {
			throw std::runtime_error("Couldn't create graphics pipeline.");
		}

		vkDestroyShaderModule(device, vertexShader, nullptr);
		vkDestroyShaderModule(device, fragmentShader, nullptr);
	}

	void createFrameBuffers() {
		swapChainFrameBuffers.resize(swapChainImageViews.size());

		for (size_t i = 0; i < swapChainImageViews.size(); i++) {
			VkFramebufferCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			createInfo.renderPass = renderPass;
			createInfo.attachmentCount = 1;
			createInfo.pAttachments = &swapChainImageViews[i];
			createInfo.width = swapChainExtent.width;
			createInfo.height = swapChainExtent.height;
			createInfo.layers = 1;

			if (vkCreateFramebuffer(device, &createInfo, nullptr, &swapChainFrameBuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("Couldn't create frame buffer.");
			}
		}
	}

	void createCommandPool() {
		QueueFamilyIndices qfis = findQueueFamilies(physicalDevice);

		VkCommandPoolCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		createInfo.queueFamilyIndex = qfis.graphicsFamily.value();

		if (vkCreateCommandPool(device, &createInfo, nullptr, &commandPool) != VK_SUCCESS) {
			throw std::runtime_error("Command buffer creation failed");
		}
	}

	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
		VkPhysicalDeviceMemoryProperties memProperties = {};
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
			if (typeFilter & (1 << i)) {
				if ((memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
					return i;
				}
			}
		}

		throw std::runtime_error("Failed to find a suitable memory type");
	}

	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
		VkBufferCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		createInfo.size = size;
		createInfo.usage = usage;
		createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		if (vkCreateBuffer(device, &createInfo, nullptr, &buffer) != VK_SUCCESS) {
			throw std::runtime_error("Create vertex buffer failed");
		}

		VkMemoryRequirements memReq = {};
		vkGetBufferMemoryRequirements(device, buffer, &memReq);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memReq.size;
		allocInfo.memoryTypeIndex = findMemoryType(memReq.memoryTypeBits, properties);

		if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
			throw std::runtime_error("Couldn't allocate memory on the device");
		}

		vkBindBufferMemory(device, buffer, bufferMemory, 0);
	}

	void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory) {
		VkImageCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		createInfo.imageType = VK_IMAGE_TYPE_2D;
		createInfo.extent.width = width;
		createInfo.extent.height = height;
		createInfo.extent.depth = 1;
		createInfo.mipLevels = 1;
		createInfo.arrayLayers = 1;
		createInfo.format = format;
		createInfo.tiling = tiling;
		createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		createInfo.usage = usage;
		createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.samples = VK_SAMPLE_COUNT_1_BIT;

		if (vkCreateImage(device, &createInfo, nullptr, &image) != VK_SUCCESS) {
			throw std::runtime_error("Create texture image failed.");
		}

		VkMemoryRequirements memReqs = {};
		vkGetImageMemoryRequirements(device, image, &memReqs);

		VkMemoryAllocateInfo allocateInfo = {};
		allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocateInfo.allocationSize = memReqs.size;
		allocateInfo.memoryTypeIndex = findMemoryType(memReqs.memoryTypeBits, properties);

		if (vkAllocateMemory(device, &allocateInfo, nullptr, &imageMemory) != VK_SUCCESS) {
			throw std::runtime_error("Create texture image memory failed.");
		}

		vkBindImageMemory(device, image, imageMemory, 0);
	}

	// Returns true if any pixel changed value (i.e., need to update)
	bool modifyGameboyTilemap(stbi_uc* pixels, int width, int height) {
		bool changed = false; 

		// We actually force this
		int channels = 4;

		// Y rows of X cols of pixels
		//   Each pixel is C channels (RR GG BB AA) or (RR GG BB)
		// TODO: We can *definitely* speed this up.
		std::vector<unsigned char> tiles = cpu->getTilemap(); // TODO: check return size

		// Draw each tile
		const size_t numTiles = tiles.size() / 64;
		for (size_t tileId = 0; tileId < numTiles; tileId++) {
			if (!cpu->isFlagChanged(tileId)) { continue; }

			// Get the destination pixel
			size_t destStartX = (tileId % 0x10) * 8;
			size_t destStartY = (tileId / 0x10) * 8;
			size_t destStartPixel = (destStartY * (width)+destStartX) * channels;

			// Copy each source pixel
			size_t i = tileId * 64;
			for (size_t y = 0; y < 8; y++) {
				for (size_t x = 0; x < 8; x++) {
					unsigned char pal = tiles[i];

					// For now we don't guess the palette
					RGB clr = { 0x00, 0xFF, 0x00 };
					switch (pal) {
					  case 0: clr = GB_PAL_WHITE; break;
					  case 1: clr = GB_PAL_LGRAY; break;
					  case 2: clr = GB_PAL_DGRAY; break;
					  case 3: clr = GB_PAL_BLACK; break;
					}

					// Get the destination position
					size_t dstPixel = destStartPixel + (x*channels);

					// Mark if anything changed
					if (!changed) {
						if (pixels[dstPixel] != clr.red) { changed = true;  }
						if (pixels[dstPixel + 1] != clr.green) { changed = true; }
						if (pixels[dstPixel + 2] != clr.blue) { changed = true; }
						if (channels > 3) { if (pixels[dstPixel + 3] != 0xFF) { changed = true; } }
					}

					// Now update it
					pixels[dstPixel] = clr.red;
					pixels[dstPixel+1] = clr.green;
					pixels[dstPixel+2] = clr.blue;
					if (channels > 3) {
						pixels[dstPixel + 3] = 0xFF; // alpha
					}

					i += 1;
				}

				// Update row
				destStartPixel += width * channels;
			}
		}

		// Reset dirty flags
		cpu->clearTilemapFlags();

		return changed;
	}

	void createTextureImages() {
		int texChannels;
		pixels = stbi_load("textures/some_pic.png", &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
		std::cout << "Found image with: " << texWidth << "x" << texHeight << ", channels: " << texChannels << std::endl;
		if (!pixels) {
			throw std::runtime_error("Failed to load texture image");
		}

		// TEMP: re-write the tilemap with the current Game Boy tileset.
		modifyGameboyTilemap(pixels, texWidth, texHeight);
		// END_TEMP

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		VkDeviceSize imageSize = texWidth * texHeight * 4;
		createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

		void* data;
		{
			vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
			memcpy(data, pixels, static_cast<size_t>(imageSize));
			vkUnmapMemory(device, stagingBufferMemory);
		}
		//stbi_image_free(pixels); //note: we are holding on to these for later.

		createImage(static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight), VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, textureImage, textureImageMemory);

		transitionLayoutImage(textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		copyBufferToImage(stagingBuffer, textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
		transitionLayoutImage(textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}




	void updateTextureImages() {
		// TEMP: re-write the tilemap with the current Game Boy tileset.
		// TODO: This is super slow!
		bool changed = modifyGameboyTilemap(pixels, texWidth, texHeight);
		if (!changed) { return;  }
		// END_TEMP

		std::cout << "Pixels changed; upddating texture in Vulkan" << std::endl;

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		VkDeviceSize imageSize = texWidth * texHeight * 4;
		createBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

		void* data;
		{
			vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
			memcpy(data, pixels, static_cast<size_t>(imageSize));
			vkUnmapMemory(device, stagingBufferMemory);
		}

		// NOTE: source of "VK_IMAGE_LAYOUT_UNDEFINED" means we don't need the original image data to be preserved.
		transitionLayoutImage(textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		copyBufferToImage(stagingBuffer, textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
		transitionLayoutImage(textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
		
		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}



	VkImageView createImageView(VkImage image, VkFormat format) {
		VkImageViewCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = image;

		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = format;

		/*createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;*/

		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		VkImageView res;
		if (vkCreateImageView(device, &createInfo, nullptr, &res) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create texture image views");
		}

		return res;
	}

	void createTextureImageView() {
		textureImageView = createImageView(textureImage, VK_FORMAT_R8G8B8A8_UNORM);
	}

	void createTextureSampler() {
		VkSamplerCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;

		auto useFilter = VK_FILTER_NEAREST;
		createInfo.magFilter = useFilter;
		createInfo.minFilter = useFilter;

		createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;

		createInfo.anisotropyEnable = false;

		createInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;

		createInfo.unnormalizedCoordinates = VK_TRUE;

		createInfo.compareEnable = VK_FALSE;
		createInfo.compareOp = VK_COMPARE_OP_ALWAYS;

		createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
		createInfo.mipLodBias = 0;
		createInfo.minLod = 0;
		createInfo.maxLod = 0;

		if (vkCreateSampler(device, &createInfo, nullptr, &textureSampler) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create sampler");
		}



	}

	VkCommandBuffer beginSingleTimeCommands() {
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = commandPool;
		allocInfo.commandBufferCount = 1;

		VkCommandBuffer commandBuffer = {};
		vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

		VkCommandBufferBeginInfo bInfo = {};
		bInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		bInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		vkBeginCommandBuffer(commandBuffer, &bInfo);

		return commandBuffer;
	}

	void endSingleTimeCommands(VkCommandBuffer buffer) {
		vkEndCommandBuffer(buffer);

		VkSubmitInfo sInfo = {};
		sInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		sInfo.commandBufferCount = 1;
		sInfo.pCommandBuffers = &buffer;

		vkQueueSubmit(graphicsQueue, 1, &sInfo, VK_NULL_HANDLE);
		vkQueueWaitIdle(graphicsQueue);

		vkFreeCommandBuffers(device, commandPool, 1, &buffer);
	}

	void transitionLayoutImage(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout) {
		VkCommandBuffer buffer = beginSingleTimeCommands();

		VkImageMemoryBarrier barrier = {};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout;
		barrier.newLayout = newLayout;

		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

		barrier.image = image;
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = 1;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;

		VkPipelineStageFlags srcStage;
		VkPipelineStageFlags dstStage;

		if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
			srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		} else {
			throw std::runtime_error("Unsupported transfer pair in transitionLayoutImage()");
		}


		vkCmdPipelineBarrier(
			buffer,
			srcStage, dstStage,
			0,
			0, nullptr,
			0, nullptr,
			1, &barrier);


		endSingleTimeCommands(buffer);
	}

	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
		VkCommandBuffer cmdBuffer = beginSingleTimeCommands();

		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;

		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;

		region.imageOffset = { 0, 0, 0 };
		region.imageExtent = {
			width, height, 1
		};

		vkCmdCopyBufferToImage(cmdBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

		endSingleTimeCommands(cmdBuffer);
	}

	void createVertexBuffers() {
		size_t size = sizeof(vertices[0]) * vertices.size();

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

		void* data;
		{
			vkMapMemory(device, stagingBufferMemory, 0, size, 0, &data);
			memcpy(data, vertices.data(), static_cast<size_t>(size));
			vkUnmapMemory(device, stagingBufferMemory);
		}

		createBuffer(size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);

		copyBuffer(stagingBuffer, vertexBuffer, size);

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}


	// TEMP!!!!
	void updateVertexBuffers() {
return;
		size_t size = sizeof(vertices[0]) * vertices.size();

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

		void* data;
		{
			vkMapMemory(device, stagingBufferMemory, 0, size, 0, &data);
			memcpy(data, vertices.data(), static_cast<size_t>(size));
			vkUnmapMemory(device, stagingBufferMemory);
		}

		//createBuffer(size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);

		copyBuffer(stagingBuffer, vertexBuffer, size);

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}


	void createIndexBuffers() {
		size_t size = sizeof(indices[0]) * indices.size();

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

		void* data;
		{
			vkMapMemory(device, stagingBufferMemory, 0, size, 0, &data);
			memcpy(data, indices.data(), static_cast<size_t>(size));
			vkUnmapMemory(device, stagingBufferMemory);
		}

		createBuffer(size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

		copyBuffer(stagingBuffer, indexBuffer, size);

		vkDestroyBuffer(device, stagingBuffer, nullptr);
		vkFreeMemory(device, stagingBufferMemory, nullptr);
	}


	void createUniformBuffers() {
		size_t size = sizeof(UniformBufferObject);
		uniformBuffers.resize(swapChainImages.size());
		uniformBuffersMemory.resize(swapChainImages.size());

		for (size_t i = 0; i < swapChainImages.size(); i++) {
			createBuffer(size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffers[i], uniformBuffersMemory[i]);
		}
	}

	void createDescriptorPool() {
		std::array<VkDescriptorPoolSize, 2> poolSizes = {};
		poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes[0].descriptorCount = static_cast<uint32_t>(swapChainImages.size());
		poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[1].descriptorCount = static_cast<uint32_t>(swapChainImages.size());

		VkDescriptorPoolCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		createInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
		createInfo.pPoolSizes = poolSizes.data();
		createInfo.maxSets = static_cast<uint32_t>(swapChainImages.size());

		if (vkCreateDescriptorPool(device, &createInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
			throw std::runtime_error("Descriptor pool failed to be created.");
		}
	}

	void createDescriptorSets() {
		std::vector<VkDescriptorSetLayout> layouts(swapChainImages.size(), descriptorSetLayout);

		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = descriptorPool;
		allocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size());
		allocInfo.pSetLayouts = layouts.data();

		descriptorSets.resize(swapChainImages.size());
		if (vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data())!= VK_SUCCESS) {
			throw std::runtime_error("Couldn't create descriptor sets.");
		}

		for (size_t i = 0; i < swapChainImages.size(); i++) {
			VkDescriptorBufferInfo buffInfo = {};
			buffInfo.buffer = uniformBuffers[i];
			buffInfo.offset = 0;
			buffInfo.range = sizeof(UniformBufferObject);

			VkDescriptorImageInfo imageInfo = {};
			imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageInfo.imageView = textureImageView;
			imageInfo.sampler = textureSampler;

			std::array<VkWriteDescriptorSet, 2> writeSets = {};
			writeSets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			writeSets[0].dstSet = descriptorSets[i];
			writeSets[0].dstBinding = 0;
			writeSets[0].dstArrayElement = 0;
			writeSets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			writeSets[0].descriptorCount = 1;
			writeSets[0].pBufferInfo = &buffInfo;

			writeSets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			writeSets[1].dstSet = descriptorSets[i];
			writeSets[1].dstBinding = 1;
			writeSets[1].dstArrayElement = 0;
			writeSets[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			writeSets[1].descriptorCount = 1;
			writeSets[1].pImageInfo = &imageInfo;
			
			vkUpdateDescriptorSets(device,static_cast<uint32_t>(writeSets.size()), writeSets.data(), 0, nullptr);

		}

	}

	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
		VkCommandBuffer buffer = beginSingleTimeCommands();

		VkBufferCopy copyRegion = {};
		copyRegion.size = size;
		vkCmdCopyBuffer(buffer, srcBuffer, dstBuffer, 1, &copyRegion);

		endSingleTimeCommands(buffer);
	}

	void createCommandBuffers() {
		commandBuffers.resize(swapChainFrameBuffers.size());

		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = commandPool;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());

		if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create command buffers");
		}


		for (size_t i = 0; i < commandBuffers.size(); i++) {
			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

			if (vkBeginCommandBuffer(commandBuffers[i], &beginInfo) != VK_SUCCESS) {
				throw std::runtime_error("Failed to begin recording command buffer");
			}

			VkRenderPassBeginInfo renderInfo = {};
			renderInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderInfo.renderPass = renderPass;
			renderInfo.framebuffer = swapChainFrameBuffers[i];
			renderInfo.renderArea.offset = { 0,0 };
			renderInfo.renderArea.extent = swapChainExtent;

			VkClearValue clearColor = { 0.4f, 0.6f, 0.9f, 1.0f };
			renderInfo.clearValueCount = 1;
			renderInfo.pClearValues = &clearColor;

			vkCmdBeginRenderPass(commandBuffers[i], &renderInfo, VK_SUBPASS_CONTENTS_INLINE);
			vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

			VkDeviceSize offset = 0;
			vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, &vertexBuffer, &offset);
			vkCmdBindIndexBuffer(commandBuffers[i], indexBuffer, 0, VK_INDEX_TYPE_UINT16);

			vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets[i], 0, nullptr);
			vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
			vkCmdEndRenderPass(commandBuffers[i]);

			if (vkEndCommandBuffer(commandBuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("Failed to end recording of command buffer");
			}
		}	
	}

	void createSemaphoresAndFences() {
		imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
		inflightFences.resize(MAX_FRAMES_IN_FLIGHT);

		VkSemaphoreCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		VkFenceCreateInfo createInfoFence = {};
		createInfoFence.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		createInfoFence.flags = VK_FENCE_CREATE_SIGNALED_BIT;

		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
			if (
				(vkCreateSemaphore(device, &createInfo, nullptr, &imageAvailableSemaphores[i]) != VK_SUCCESS) ||
				(vkCreateSemaphore(device, &createInfo, nullptr, &renderFinishedSemaphores[i]) != VK_SUCCESS) ||
				(vkCreateFence(device, &createInfoFence, nullptr, &inflightFences[i]) != VK_SUCCESS)
			) {
				throw std::runtime_error("Semaphore/fence creation failed.");
			}
		}
	}

	VkShaderModule createShaderModule(const std::vector<char>& src) {
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = src.size();
		createInfo.pCode = reinterpret_cast<const uint32_t*>(src.data());

		VkShaderModule res;
		if (vkCreateShaderModule(device, &createInfo, nullptr, &res) != VK_SUCCESS) {
			throw std::runtime_error("Failed to create shader module");
		}
		return res;
	}


	void mainLoop() {
		while (!glfwWindowShouldClose(window)) {
			auto startTime = std::chrono::high_resolution_clock::now();

			glfwPollEvents();

			if (REALTIME) {
				updateEmulator();
			}

			drawFrame();

			// Time to wait?
			long diffTimeMS = static_cast<long>((1000.0f/60) - std::chrono::duration<float, std::chrono::milliseconds::period>(std::chrono::high_resolution_clock::now() - startTime).count());
			if (diffTimeMS > 0) {
				std::this_thread::sleep_for(std::chrono::milliseconds(diffTimeMS));
			}
		}
	}


	std::map<GB_BUTTONS, bool> getKeyInput() {
		std::map<GB_BUTTONS, bool> res;

		res[GB_BUTTONS::A] = (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS);
		res[GB_BUTTONS::B] = (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS);
		res[GB_BUTTONS::SELECT] = (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS);
		res[GB_BUTTONS::START] = (glfwGetKey(window, GLFW_KEY_ENTER) == GLFW_PRESS);
		res[GB_BUTTONS::RIGHT] = (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS);
		res[GB_BUTTONS::LEFT] = (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS);
		res[GB_BUTTONS::UP] = (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS);
		res[GB_BUTTONS::DOWN] = (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS);

		return res;
	}


	void updateEmulator() {
		// Get input
		latestButtons = getKeyInput();

		// Run the CPU until vblank
		cpu->setKeyState(latestButtons);
		cpu->runUntilNextVBlank();

		// For now, just re-send all vertex data every frame.
		//get_vertices_and_indices(vertices, indices);
		updateTextureImages();
		updateVertexBuffers();
	}


	void updateUniformBuffer(uint32_t currentImage) {
		static auto startTime = std::chrono::high_resolution_clock::now();
		auto currTime = std::chrono::high_resolution_clock::now();
		float time = std::chrono::duration<float, std::chrono::seconds::period>(currTime - startTime).count();

		UniformBufferObject ubo = {};

		// Save the width/height of the swap chain and the actual map itself
		ubo.win.x = static_cast<float>(swapChainExtent.width);
		ubo.win.y = static_cast<float>(swapChainExtent.height);
		ubo.map.x = 64 * 8;
		ubo.map.y = 64 * 6;

		// Update the tile data
		auto tiles = cpu->getBgTileMap();
		for (size_t i = 0; i < 0x400; i++) {
			if (i < tiles.size()) {
				ubo.tiles[i/4][i%4] = tiles[i];
			} else {
				ubo.tiles[i/4][i%4] = 0;
			}
		}

		// Update the sprite data
		auto sprites = cpu->getSprites();
		for (size_t i = 0; i < 40; i++) {
			// TODO: update x and y (maybe not here?)
			ubo.sprites[i][0] = -8;
			ubo.sprites[i][1] = -16;
			ubo.sprites[i][2] = 0;
			ubo.sprites[i][3] = 0;
			if (i < sprites.size()) {
				const auto& spr = sprites[i];
				ubo.sprites[i][0] += std::get<1>(spr); // Note: GB flips x/y
				ubo.sprites[i][1] += std::get<0>(spr);
				ubo.sprites[i][2] = std::get<2>(spr);
			}
		}

		// Update our button data
		ubo.buttons[0][0] = latestButtons[GB_BUTTONS::LEFT]; // Left
		ubo.buttons[1][0] = latestButtons[GB_BUTTONS::RIGHT]; // Right
		ubo.buttons[2][0] = latestButtons[GB_BUTTONS::UP]; // Up
		ubo.buttons[3][0] = latestButtons[GB_BUTTONS::DOWN]; // Down
		ubo.buttons[4][0] = latestButtons[GB_BUTTONS::SELECT]; // Select
		ubo.buttons[5][0] = latestButtons[GB_BUTTONS::START]; // Start
		ubo.buttons[6][0] = latestButtons[GB_BUTTONS::B]; // B
		ubo.buttons[7][0] = latestButtons[GB_BUTTONS::A]; // A

		void* data;
		{
			vkMapMemory(device, uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
			memcpy(data, &ubo, sizeof(ubo));
			vkUnmapMemory(device, uniformBuffersMemory[currentImage]);
		}
	}

	void drawFrame() {
		vkWaitForFences(device, 1, &inflightFences[currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());

		uint32_t imageIndex = 0;
		VkResult res = vkAcquireNextImageKHR(device, swapChain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);
		if (res == VK_ERROR_OUT_OF_DATE_KHR) {
			recreateSwapChain();
			return;
		} else if ((res != VK_SUCCESS) && (res != VK_SUBOPTIMAL_KHR)) {
			throw std::runtime_error("Draw frame failed.");
		}

		updateUniformBuffer(imageIndex);

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		VkPipelineStageFlags waitStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &imageAvailableSemaphores[currentFrame];
		submitInfo.pWaitDstStageMask = &waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &renderFinishedSemaphores[currentFrame];

		// Reset the fence right before we use it; otherwise, the "return" can mess us up.
		vkResetFences(device, 1, &inflightFences[currentFrame]);
		if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, inflightFences[currentFrame]) != VK_SUCCESS) {
			throw std::runtime_error("Faile dto submit draw frame queue");
		}

		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &renderFinishedSemaphores[currentFrame];
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &swapChain;
		presentInfo.pImageIndices = &imageIndex;

		res = vkQueuePresentKHR(graphicsQueue, &presentInfo);
		if ((res == VK_ERROR_OUT_OF_DATE_KHR) || (res == VK_SUBOPTIMAL_KHR) || framebufferResized){
			framebufferResized = false;
			recreateSwapChain();
			return;
		} else if (res != VK_SUCCESS) {
			throw std::runtime_error("Draw frame failed.");
		}

		currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
	}

	void cleanupVulkan() {
		cleanupSwapChain();

		vkDestroySampler(device, textureSampler, nullptr);
		vkDestroyImageView(device, textureImageView, nullptr);
		vkDestroyImage(device, textureImage, nullptr);
		vkFreeMemory(device, textureImageMemory, nullptr);

		vkDestroyDescriptorPool(device, descriptorPool, nullptr);

		vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);

		for (size_t i = 0; i < uniformBuffers.size(); i++) {
			vkDestroyBuffer(device, uniformBuffers[i], nullptr);
			vkFreeMemory(device, uniformBuffersMemory[i], nullptr);
		}

		vkDestroyBuffer(device, vertexBuffer, nullptr);
		vkFreeMemory(device, vertexBufferMemory, nullptr);

		vkDestroyBuffer(device, indexBuffer, nullptr);
		vkFreeMemory(device, indexBufferMemory, nullptr);


		for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
			vkDestroySemaphore(device, imageAvailableSemaphores[i], nullptr);
			vkDestroySemaphore(device, renderFinishedSemaphores[i], nullptr);
			vkDestroyFence(device, inflightFences[i], nullptr);
		}
		vkDestroyCommandPool(device, commandPool, nullptr);		

		vkDestroyDevice(device, nullptr);

		if (enableValidationLayers) {
			DestroyDebugUtilsMessengerEXT(instance, callback, nullptr);
		}

		vkDestroySurfaceKHR(instance, surface, nullptr);
		vkDestroyInstance(instance, nullptr);
	}

	void cleanupWindow() {
		if (window) {
			glfwDestroyWindow(window);
		}

		glfwTerminate();
	}

private:
	VkInstance instance;
	VkDebugUtilsMessengerEXT callback;

	VkPhysicalDevice physicalDevice;
	VkDevice device;

	std::string title;
	GLFWwindow* window;
	VkSurfaceKHR surface;

	VkQueue graphicsQueue;
	VkQueue presentQueue;

	VkSwapchainKHR swapChain;
	std::vector<VkImage> swapChainImages;
	std::vector<VkImageView> swapChainImageViews;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;

	VkRenderPass renderPass;
	VkDescriptorSetLayout descriptorSetLayout;
	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;

	std::vector<VkFramebuffer> swapChainFrameBuffers;

	VkCommandPool commandPool;
	std::vector<VkCommandBuffer> commandBuffers;

	VkBuffer vertexBuffer;
	VkDeviceMemory vertexBufferMemory;
	VkBuffer indexBuffer;
	VkDeviceMemory indexBufferMemory;

	std::vector<VkBuffer> uniformBuffers;
	std::vector<VkDeviceMemory> uniformBuffersMemory;

	VkDescriptorPool descriptorPool;
	std::vector<VkDescriptorSet> descriptorSets;

	VkImage textureImage;
	VkDeviceMemory textureImageMemory;
	VkImageView textureImageView;
	VkSampler textureSampler;

	std::vector<VkSemaphore> imageAvailableSemaphores;
	std::vector<VkSemaphore> renderFinishedSemaphores;
	std::vector<VkFence> inflightFences;

	size_t currentFrame;
	std::map<GB_BUTTONS, bool> latestButtons;

	stbi_uc* pixels = 0; // dynamic set of pixels
	int texWidth, texHeight; // width/height of the pixels
	
	bool framebufferResized;
};


int main(int argc, char *argv[]) {
	// Get the name of the ROM
	std::string game_name = DEFAULT_GAME;
	if (argc>1) {
		game_name = argv[1];
	}

	// Temp: run the ROM to a breakpoint, and then just print the tiles.
	cpu = std::make_unique<GB_CPU>(0xCC);

	// Try to figure out what's going on here... it should only trigger once.
	cpu->traceProgramFlowTo(0x21B);

	// Note: Some handy breakpoints for Tetris. They're all disabled in realtime mode, anyway.
	cpu->setBreakpoint(0x7ff0); // NOTE: We can display tiles at this point!
	cpu->setBreakpoint(0x408); // NOTE: Tetris logo loaded in tiles. ( 0x442EA9 cycles later )
	cpu->setBreakpoint(0x3A0); // Poll recently pressed buttons
	cpu->setBreakpoint(0x3A9); // "something" pressed on first screen
	cpu->setBreakpoint(0x4B6); // "Start" only pressed on the second (title) screen
	cpu->setBreakpoint(0x1766); // Game Options screen showing
	
	// If you want to "play" the game
	cpu->setRealtime(REALTIME);

	cpu->runFile(game_name);

	if (!REALTIME) {
		cpu->printAllRam();
	}

	// Get title
	std::string title = cpu->getROMName();
	if (title.empty()) {
		title = "GB-CPU Program";
	}

	// Setup
	get_vertices_and_indices(vertices, indices);
	SampleApp app(title);

	try {
		app.run();
	} catch (const std::exception& ex) {
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}


