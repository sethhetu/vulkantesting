#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec2 inTexCoord;
layout(location = 2) in uvec2 tileData; // x = use, y = tileId

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragTexCoord;

// win = width/height of current window (swapChain image)
// map = width/height of tile map we're presenting
layout(binding = 0, std140) uniform UniformBufferObject {
  vec2 win;
  vec2 map;
  uvec4 tiles[0x100]; // 0x100*4 tiles
  ivec4 sprites[0x28];  // 40 sprites (x,y,tileId, unused)
  ivec4 buttons[8];  // 8 buttons ("visible", unused, unused, unused)
} ubo;

out gl_PerVertex {
  vec4 gl_Position;
};

void main(){
  // We're operating on a world view of 0..64*8, 0..64*6
  // I.e., 8 tiles by 6 tiles, in 64 pixel tiles
  gl_Position = vec4(inPosition, 0.0, 1.0);

  // Sprites/buttons override this
  if (tileData.x == 2) {
    gl_Position[0] += ubo.sprites[tileData.y][0];
	gl_Position[1] += ubo.sprites[tileData.y][1];
  } else if (tileData.x == 3) {
    // We move the button highlight offscreen if it's marked "invisible".
	if (ubo.buttons[tileData.y][0] == 0) {
		gl_Position[0] = -900;
		gl_Position[1] = -900;
	}
  }

  // Transform world to clip coords
  // Multiply by 2 since we're scaling from 0..1 instead of -1..1
  gl_Position.x = (2.0*gl_Position.x) / ubo.win.x;
  gl_Position.y = (2.0*gl_Position.y) / ubo.win.y;

  // Center it. /2 is not needed, due to the 0..1 scaling we did earlier.
  gl_Position.x -= ubo.map.x / ubo.win.x;
  gl_Position.y -= ubo.map.y / ubo.win.y;

  // Pass these along to the fragment shader
  // TODO: We can modify this based on the current tile ID??
  // TODO: Currently just re-using a flag to track this.
  fragTexCoord = inTexCoord;
  if (tileData.x == 1) {
    uint tileID = ubo.tiles[tileData.y/4][tileData.y%4];
	fragTexCoord.x += 8*(tileID%0x10);
	fragTexCoord.y += 8*floor(tileID/0x10);
  } else if (tileData.x == 2) {
    uint tileID = ubo.sprites[tileData.y][2];
	fragTexCoord.x += 8*(tileID%0x10);
	fragTexCoord.y += 8*floor(tileID/0x10);
  }

  // Unused, but pass along for now
  fragColor = vec3(0,0,0);
}



